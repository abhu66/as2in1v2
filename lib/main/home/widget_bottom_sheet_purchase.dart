
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetBottomSheetPurchase extends StatefulWidget {


  @override
  _WidgetBottomSheetPurchaseState createState() => _WidgetBottomSheetPurchaseState();
}

class _WidgetBottomSheetPurchaseState extends State<WidgetBottomSheetPurchase> {
  @override
  Widget build(BuildContext context) {
    return widgetGridMenuPurchase();
  }

  Widget widgetGridMenuPurchase(){
    return GridView.count(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      padding: EdgeInsets.all(10),
        crossAxisCount: 4,
        crossAxisSpacing: 4.0,
        mainAxisSpacing: 8.0,
        children: List.generate(
            choicesPurchase.length, (index) {
          return Center(
            child: SelectCard(choice: choicesPurchase[index],index: index,key:UniqueKey(),),
          );
        }
        )
    );
  }
}

class Choice {
  const Choice({required this.title, required this.icon});
  final String title;
  final String icon;
}

const List<Choice> choicesPurchase = const <Choice>[
  const Choice(title: 'Transfer Balance', icon: "assets/purchase/balance_transfer_v2.png"),
  const Choice(title: 'Transfer Data Package', icon: "assets/purchase/transfer_data_package.png"),
  const Choice(title: 'Transfer to Malaysia', icon: "assets/purchase/transfer_to_malaysia.png"),
  const Choice(title: 'PLN Prepaid', icon: "assets/purchase/pln_prepaid.png"),
  const Choice(title: 'Voice Package', icon: "assets/purchase/voice_package.png"),
  const Choice(title: 'Alfamart Voucher', icon: "assets/purchase/alfamart_v2.png"),
  const Choice(title: 'Indomaret Voucher', icon: "assets/purchase/indomaret_v2.png"),
  const Choice(title: 'SMS Package', icon:"assets/purchase/sms_package.png"),
  const Choice(title: 'Caller Ring Back Tone', icon:"assets/purchase/caller_ring_back_tone.png"),
  const Choice(title: 'As2in1 App Number', icon: "assets/purchase/as2in1_app_number.png"),
  const Choice(title: 'Wifi in Indonesia', icon: "assets/purchase/wifi.png"),
  const Choice(title: 'DDHK-Infaq', icon: "assets/purchase/DDHK_infaq.png"),
  const Choice(title: 'Subscribe P3K', icon: "assets/purchase/subscribe_p3k.png"),
];


class SelectCard extends StatelessWidget {
  const SelectCard({required Key key, required this.choice,required this.index}) : super(key: key);
  final Choice choice;
  final int index;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = TextStyle(color: Colors.black, fontSize: 12.0);
    return InkWell(
      child: Container(
        padding: EdgeInsets.all(5),
          child: Center(child: ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                Image.asset(choice.icon,width: 40,height: 40),
                SizedBox(height: 10,),
                Text(choice.title, style: textStyle,textAlign: TextAlign.center,),
                SizedBox(height: 10,),
              ]
          ),
          )
      ),
      onTap: (){
        showModalBottomSheet(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(30),
                topLeft: Radius.circular(30)
                  ),
            ),
            backgroundColor: Colors.white,
            isScrollControlled: true,
            context: context, builder: (BuildContext context){
          return Container(
              height: MediaQuery.of(context).size.height - AppBar().preferredSize.height,
              child: ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  InkWell(
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Spacer(),
                          RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                                text:choice.title,
                                style:TextStyle(color: Colors.black, fontSize: 18.0),
                            ),
                          ),
                          Spacer(),
                          Icon(Icons.clear,size: 35,)
                        ],
                      ),
                      margin: EdgeInsets.all(20),
                    ),
                    onTap: (){
                      Navigator.of(context).pop();
                    },
                  ),
                  //setWidgetScreen(index: index)
                ],
              ));
        }
        );
      },
    );
  }

  // Widget setWidgetScreen({int index}){
  //   switch(index){
  //     case 0 :
  //       return   WidgetBottomSheetTransferBalance(); break;
  //     case 1 :
  //       return   WidgetBottomSheetTransferDataPackage(); break;
  //     default : break;
  //   }
  // }
}
