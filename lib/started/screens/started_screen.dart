import 'dart:async';
import 'package:as2in1_app/const/colors_const.dart';
import 'package:as2in1_app/const/const_string.dart';
import 'package:as2in1_app/started/screens/faq_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StartedScreen extends StatefulWidget {
  @override
  _StartedScreenState createState() => _StartedScreenState();
}

class _StartedScreenState extends State<StartedScreen> {
  double opacityLevel = 0.0;
  @override
  void initState() {
    super.initState();
    //startSplashScreen();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Image.asset("assets/ic_slice_top.png",fit: BoxFit.cover,width: 250),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(left: 50,right: 50),
              child:Center(
                child: ListView(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  children: [
                    Image.asset("assets/ic_splash.png",fit: BoxFit.cover),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          text:'${ConstStringBahasa.welcome}',
                          style: TextStyle(fontSize: 20,color: Colors.black),
                      ),
                    ),
                    SizedBox(height: 10,),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          text:'${ConstStringBahasa.welcomeSub}',
                        style: TextStyle(fontSize: 14,color: Colors.black),
                      ),
                    ),
                  ],
                ),
              ),
          ),
          Positioned(
              bottom: 30,
              right: 50,
              left: 50,
              child: buttonNext()),
        ],
      ),
    );
  }

  Widget buttonNext(){
    return InkWell(
      onTap: (){
        goToFAQ();
      },
      child: Container(
        height: 45,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: HexColor(COLOR_RED),
        ),
        child: Center(
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                text:'${ConstStringBahasa.started}',
            ),
          ),
        ),
      ),
    );
  }



  startSplashScreen() async {
    var duration = const Duration(seconds: 3);
    return Timer(duration,() async {
      goToFAQ();
    });
  }

  goToFAQ() async {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (_){
        return FAQScreen();
      }),
    );
  }
}
