import 'dart:math';

import 'package:as2in1_app/const/colors_const.dart';
import 'package:as2in1_app/main/home/widget_recomendation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UpcomingEventsScreen extends StatefulWidget {
  @override
  _UpcomingEventsScreenState createState() => _UpcomingEventsScreenState();
}

class _UpcomingEventsScreenState extends State<UpcomingEventsScreen> {
  final _random = Random();
  List cardList = [Item1(), Item1(), Item1(), Item1()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        iconTheme: IconThemeData(color: Colors.white),
        centerTitle: false,
        backgroundColor: Colors.transparent,
        title: Text("Upcoming Events",style: TextStyle(fontSize: 18.0, color: Colors.white),),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                HexColor("#891F1F"),
                HexColor("#B11B33"),
              ],
            ),
          ),
        ),
      ),
      body: Container(
        child: ListView(
          physics: AlwaysScrollableScrollPhysics(),
          children: [
            ListTile(
              title: Text("UPCOMING",style: TextStyle(fontSize: 14.0, color: Colors.black,fontWeight: FontWeight.bold),),
              subtitle: Text("EVENTS",style: TextStyle(fontSize: 20.0, color: Colors.black,fontWeight: FontWeight.bold),),
              trailing: Text("${DateTime.now().year}",style: TextStyle(fontSize: 20.0, color: Colors.black,fontWeight: FontWeight.bold,
                  decoration: TextDecoration.underline),),
            ),
            ListView.builder(
                shrinkWrap: true,
                itemCount: cardList.length,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, int index){
              return Container(
                margin: EdgeInsets.only(right: 10,top: 2),
                decoration: BoxDecoration(
                  color: HexColor("#F0F8FF"),
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: index % 2 == 0 ? ListTile(
                    dense: true,
                    leading: Container(
                      height: 30,
                      width: 80,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.primaries[
                        _random.nextInt(Colors.primaries.length)]
                        [_random.nextInt(9) * 100],
                      ),
                      child: Center(
                        child: RichText(
                          textAlign: TextAlign.left,
                          text: TextSpan(
                            text: "AUG 10",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ),
                    ),
                    title: RichText(
                      textAlign: TextAlign.left,
                      text: TextSpan(
                        text: "Volunteer Board Meeting".toUpperCase(),
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                    subtitle: RichText(
                      textAlign: TextAlign.left,
                      text: TextSpan(
                        text: "LOREM IPSUM DOLOR SIT AMET CONSECTETUR",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 10,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ) : ListTile(
                    dense: true,
                    trailing: Container(
                      height: 30,
                      width: 80,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.primaries[
                        _random.nextInt(Colors.primaries.length)]
                        [_random.nextInt(9) * 100],
                      ),
                      child: Center(
                        child: RichText(
                          textAlign: TextAlign.left,
                          text: TextSpan(
                            text: "AUG 10",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ),
                    ),
                    title: RichText(
                      textAlign: TextAlign.left,
                      text: TextSpan(
                        text: "Volunteer Board Meeting".toUpperCase(),
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                    subtitle: RichText(
                      textAlign: TextAlign.left,
                      text: TextSpan(
                        text: "LOREM IPSUM DOLOR SIT AMET CONSECTETUR",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 10,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                ),
              );
            }),
          ],
        ),
      ),
    );
  }
}
