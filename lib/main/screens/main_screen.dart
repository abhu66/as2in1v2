import 'package:as2in1_app/const/colors_const.dart';
import 'package:as2in1_app/main/account/screens/account_screen.dart';
import 'package:as2in1_app/main/chat/chat_screen.dart';
import 'package:as2in1_app/main/home/home_screen.dart';
import 'package:as2in1_app/main/home/keypad.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedTabIndex = 0;


  _changeIndex(int index) {
    setState(() {
      _selectedTabIndex = index;
    });
  }

  List _pages = [
    HomeScreen(),
    ChatScreen(),
    Keypad(),
    Text("Contact"),
    AccountScreen(),
  ];

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      backgroundColor: HexColor("#F0F8FF"),
      body: Center(child: _pages[_selectedTabIndex]),
      bottomNavigationBar:  BottomAppBar(
        elevation: 1,
        shape: CircularNotchedRectangle(),
        notchMargin: 8.0,
        clipBehavior: Clip.antiAlias,
        child: Container(
          color: HexColor("#E5E5E5"),
          height: kBottomNavigationBarHeight,
          child: Container(
            child: BottomNavigationBar(
              elevation: 0.0,
              type: BottomNavigationBarType.fixed,
                currentIndex: _selectedTabIndex,
                backgroundColor: Colors.white,
                selectedItemColor: Colors.red[900],
                showUnselectedLabels: true,
                showSelectedLabels: true,
                selectedLabelStyle: TextStyle(color: Colors.red),
                onTap: (index) {
                  setState(() {
                    _selectedTabIndex = index;
                  });
                },
                items: [
                  BottomNavigationBarItem(
                      icon: Image.asset("assets/icons/home_filled.png",width: 20,height: 20,color: _selectedTabIndex == 0 ? Colors.red[900] : Colors.grey,), label: 'Home'),
                  BottomNavigationBarItem(
                      icon: Image.asset("assets/icons/chat.png",width: 20,height: 20,color: _selectedTabIndex == 1 ? Colors.red[900] : Colors.grey,), label: 'Chat'),

                  BottomNavigationBarItem(icon: Icon(Icons.dialpad_rounded), label: ''),
                  BottomNavigationBarItem(
                      icon: Image.asset("assets/icons/contacts.png",width: 20,height: 20,color: _selectedTabIndex == 3 ? Colors.red[900] : Colors.grey,), label: 'Contacts'),

                  BottomNavigationBarItem(
                      icon: Image.asset("assets/icons/user.png",width: 20,height: 20,color:_selectedTabIndex == 4 ? Colors.red[900] : Colors.grey,), label: 'Account'),

                ]),
          ),
        ),
      ),
      floatingActionButtonLocation:
      FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            gradient: LinearGradient(
              colors: [
                HexColor("#891F1F"),
                HexColor("#B11B33"),
              ],
            ),
          ),
          child: FloatingActionButton(
            backgroundColor: Colors.transparent,
            child: Icon(Icons.dialpad_rounded),
            onPressed: () => setState(() {
              _selectedTabIndex = 2;
            }),
          ),
        ),
      ),
    );
  }

}
