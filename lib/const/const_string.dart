class ConstStringBahasa {
  static String started    = "Memulai";
  static String welcome    = "Selamat Datang di As2in1";
  static String welcomeSub = "Siap menikmati chat, media, dan transfer pulsa bersama keluarga dan teman di Indonesia?";
}

class ConstStringEnglish {
  static String started    = "Get Started";
  static String welcome    = "Welcome to As2in1";
  static String welcomeSub = "Ready to enjoy chat, media and balance transfers with your family and friends in indonesia?";
}