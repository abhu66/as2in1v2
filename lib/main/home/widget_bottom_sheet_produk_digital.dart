
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetBottomSheetProdukDigital extends StatefulWidget {


  @override
  _WidgetBottomSheetProdukDigitalState createState() => _WidgetBottomSheetProdukDigitalState();
}

class _WidgetBottomSheetProdukDigitalState extends State<WidgetBottomSheetProdukDigital> {
  Widget build(BuildContext context) {
    return widgetGridMenuPurchase();
  }

  Widget widgetGridMenuPurchase(){
    return GridView.count(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.all(10),
        crossAxisCount: 4,
        crossAxisSpacing: 4.0,
        mainAxisSpacing: 8.0,
        children: List.generate(
            choicesDigital.length, (index) {
          return Center(
            child: SelectCard(choice: choicesDigital[index],index: index,key: UniqueKey(),),
          );
        }
        )
    );
  }
}

class Choice {
  const Choice({required this.title, required this.icon});
  final String title;
  final String icon;
}

const List<Choice> choicesDigital = const <Choice>[
  const Choice(title: 'Subscribe P3K', icon: "assets/topup/ic_paper_voucher.png"),
  const Choice(title: 'ID Registration', icon: "assets/topup/ic_ck.png"),
  const Choice(title: 'Special Number Purchasing', icon: "assets/topup/ic_id_topup.png"),
  const Choice(title: 'Partner Icon Redirect Link', icon: "assets/topup/ic_taiwan_topup.png"),
  const Choice(title: 'App to App Chat Message', icon: "assets/topup/ic_paypal.png"),
  const Choice(title: 'App to App Free Call & Video Call', icon: "assets/topup/ic_tng.png"),
  const Choice(title: 'DDHK-Donation', icon: "assets/topup/ic_saudi.png"),
  const Choice(title: 'App Broadcast', icon:"assets/topup/ic_srs_malay.png"),
  const Choice(title: 'Recomendation Advertising', icon:"assets/topup/ic_malay_grapari.png"),
];


class SelectCard extends StatelessWidget {
  const SelectCard({required Key key, required this.choice,required this.index}) : super(key: key);
  final Choice choice;
  final int index;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = TextStyle(fontSize: 18.0,color: Colors.black);
    return InkWell(
      child: Container(
          padding: EdgeInsets.all(5),
          child: Center(child: ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                Image.asset(choice.icon,width: 40,height: 40),
                SizedBox(height: 10,),
                Text(choice.title, style: textStyle,textAlign: TextAlign.center,),
                SizedBox(height: 10,),
              ]
          ),
          )
      ),
      onTap: (){
        showModalBottomSheet(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  topLeft: Radius.circular(30)
              ),
            ),
            backgroundColor: Colors.white,
            isScrollControlled: true,
            context: context, builder: (BuildContext context){
          return Container(
              height: MediaQuery.of(context).size.height - AppBar().preferredSize.height,
              child: ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  InkWell(
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Spacer(),
                          RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                                text:choice.title,
                                style:TextStyle(fontSize: 18.0,color: Colors.black)
                            ),
                          ),
                          Spacer(),
                          Icon(Icons.clear,size: 35,)
                        ],
                      ),
                      margin: EdgeInsets.all(20),
                    ),
                    onTap: (){
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ));
        }
        );
      },
    );
  }
}
