
import 'package:as2in1_app/main/home/widget_bottom_sheet_vidio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetBottomSheetService extends StatefulWidget {

  @override
  _WidgetBottomSheetServiceState createState() => _WidgetBottomSheetServiceState();
}

class _WidgetBottomSheetServiceState extends State<WidgetBottomSheetService> {
  @override
  Widget build(BuildContext context) {
    return widgetGridMenuPurchase();
  }

  Widget widgetGridMenuPurchase(){
    return GridView.count(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      padding: EdgeInsets.all(10),
        crossAxisCount: 4,
        crossAxisSpacing: 4.0,
        mainAxisSpacing: 8.0,
        children: List.generate(
            choicesServices.length, (index) {
          return Center(
            child: SelectCard(choice: choicesServices[index],key: UniqueKey(),),
          );
        }
        )
    );
  }
}

class Choice {
  const Choice({required this.title, required this.icon,required this.url});
  final String title;
  final String icon;
  final String url;
}

const List<Choice> choicesServices = const <Choice>[
  const Choice(title: 'Customer Service', icon: "assets/service/customer_service_v3.png",url: ""),
  const Choice(title: 'Referral Program', icon: "assets/service/referal_program.png",url: ""),
  const Choice(title: 'Apply Referral', icon: "assets/service/apply_refereal_v2.png",url: ""),
  const Choice(title: 'ID Registration', icon: "assets/service/id_registration.png",url: "https://regapps.kartuas2in1.com"),
  const Choice(title: 'Vidio', icon: "assets/service/vidio.png",url: "https://m.vidio.com"),
  const Choice(title: 'INDOSOUND', icon: "assets/service/IndoSound.png",url: "https://cast4.citrus3.com:2199/start/indosoundnetradio/"),
  const Choice(title: 'KlikDokter', icon: "assets/service/klik_dokter.png",url: "https://www.klikdokter.com/"),
  const Choice(title: 'Liputan 6', icon:"assets/service/liputan6_v2.png",url: "https://www.liputan6.com/"),
  const Choice(title: 'Bola', icon:"assets/service/bola_v2.png",url: "https://www.bola.com/"),
  const Choice(title: 'Bintang', icon: "assets/service/bintangcom.png",url: "https://www.fimela.com/"),
  const Choice(title: 'Liputan BMI', icon: "assets/service/logo.png",url: "https://www.liputanbmi.com/"),
  const Choice(title: 'Apakabar', icon: "assets/service/logo_akol_circle.png",url: "https://apakabaronline.com/"),
  const Choice(title: 'DDHK-Website', icon: "assets/service/DDHK_Website.png",url: "https://ddhk.org/"),
];


class SelectCard extends StatelessWidget {
  const SelectCard({required Key key, required this.choice}) : super(key: key);
  final Choice choice;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = TextStyle(fontSize: 18.0,color: Colors.black);
    return InkWell(
      child: Container(
        padding: EdgeInsets.all(5),
          child: Center(child: ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                Image.asset(choice.icon,width: 40,height: 40),
                SizedBox(height: 10,),
                Text(choice.title, style: textStyle,textAlign: TextAlign.center,),
                SizedBox(height: 10,),
              ]
          ),
          )
      ),
      onTap: (){
          showModalBottomSheet(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              backgroundColor: Colors.white,
              isScrollControlled: true,
              context: context, builder: (BuildContext context){
            return Container(
                height: MediaQuery.of(context).size.height - AppBar().preferredSize.height,
                child: ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    InkWell(
                      child: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Spacer(),
                            RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                  text:choice.title,
                                  style: TextStyle(fontSize: 18.0,color: Colors.black)
                              ),
                            ),
                            Spacer(),
                            Icon(Icons.clear,size: 35,)
                          ],
                        ),
                        margin: EdgeInsets.all(20),
                      ),
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                    ),

                    WidgetBottomSheetVidio(url:choice.url),

                  ],
                ));
          }
          );
      },
    );
  }
}
