
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class SharedPref{
  static read(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString(key) as String);
  }
  static readToken(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }
  static save(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }
  static saveToken(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
    print("Device Token saved" +value.toString());
  }
  static remove(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  Future<String> getToken() async {
    late String token;
    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('token')) {
      token = await SharedPref.readToken("token");
    }
    return token;
  }

  Future<String> isFirstTime() async {
    late String token;
    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('first')) {
       token = await SharedPref.readToken("first");
    }
    return token;
  }

  Future<String> isBahasa() async {
    late String bahasa;
    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('isBahasa')) {
      bahasa = await SharedPref.readToken("isBahasa");
    }
    return bahasa;
  }

  // Future<ResponseBodyLogin> loadDataLogin() async {
  //   final prefs = await SharedPreferences.getInstance();
  //   if (prefs.containsKey('login')) {
  //     ResponseBodyLogin login = ResponseBodyLogin.fromJson(await SharedPref.read("login"));
  //     return login;
  //   }
  //   else {
  //     return null;
  //   }
  // }
  //
  // Future<String> getFCMToken() async {
  //   final prefs = await SharedPreferences.getInstance();
  //   if (prefs.containsKey('deviceToken')) {
  //     String deviceToken = await SharedPref.readToken("deviceToken");
  //     return deviceToken;
  //   }
  //   else {
  //     return null;
  //   }
  // }
}