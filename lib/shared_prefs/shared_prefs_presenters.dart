





import 'package:as2in1_app/shared_prefs/shared_prefs.dart';

abstract class SharedPrefContract{
 void onLoadingShared(bool isLoading);
 void onLoadingFcmToken(bool isLoading);
 //void onSuccessProfile(Profile profile);
 void onFailure(String error);
}

class SharedPrefPresenter{
  SharedPref sharedPref = SharedPref();
  SharedPrefContract view;
  SharedPrefPresenter(this.view);
  //static User user;
  static bool loading = false;
  setLoading(bool isLoading){
    SharedPrefPresenter.loading = isLoading;
  }
  bool isLoading() {
    return SharedPrefPresenter.loading;
  }

}