
import 'package:as2in1_app/const/colors_const.dart';
import 'package:as2in1_app/started/screens/otp_screen.dart';
import 'package:as2in1_app/started/screens/register_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChooseVerificastionScreenn extends StatefulWidget {
  final String? phoneNumber;
  ChooseVerificastionScreenn({this.phoneNumber});
  @override
  _ChooseVerificastionScreennState createState() => _ChooseVerificastionScreennState();
}

class _ChooseVerificastionScreennState extends State<ChooseVerificastionScreenn> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.6,
        centerTitle: false,
        title: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
              text:'Pilih Metode Verifikasi',
              style: TextStyle(color: Colors.black,fontSize: 18),
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            child: ListView(
              padding: EdgeInsets.only(left: 20,right: 20,top: 30),
              physics: BouncingScrollPhysics(),
              children: [
                SizedBox(height: 16,),
                ListTile(
                  title: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                        text:'Pilih salah satu metode dibawah ini untuk mendapatkan kode verifikasi',
                        style: TextStyle(color: Colors.black,fontSize: 18)
                    ),
                  ),
                ),

                SizedBox(height: 30,),
                InkWell(
                  onTap: (){
                    goToOTPScreen();
                  },
                  child: Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: Colors.grey.withOpacity(0.5))
                    ),
                    child:  Center(
                      child: ListTile(
                        dense: true,
                        leading:Image.asset("assets/icons/whatsapp.png"),
                        title: RichText(
                          textAlign: TextAlign.left,
                          text: TextSpan(
                              text:'Whatsapp',
                              style: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.w700),),
                        ),
                        subtitle: RichText(
                          textAlign: TextAlign.left,
                          text: TextSpan(
                              text:'\n${widget.phoneNumber}',
                              style: Theme.of(context).textTheme.headline1?.copyWith(color: Colors.black,fontSize: 16,fontWeight: FontWeight.normal)
                          ),
                        ),
                      ),
                    ),
                  ),
                ),

                SizedBox(height: 16,),
                InkWell(
                  onTap: (){
                    goToOTPScreen();
                  },
                  child: Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.grey.withOpacity(0.5))
                    ),
                    child:  Center(
                      child: ListTile(
                        dense: true,
                        leading:Icon(Icons.chat_outlined,size: 45,color:HexColor("#25D366"),),
                        title: RichText(
                          textAlign: TextAlign.left,
                          text: TextSpan(
                            text:'SMS Ke',
                            style: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.w700),),
                        ),
                        subtitle: RichText(
                          textAlign: TextAlign.left,
                          text: TextSpan(
                              text:'\n${widget.phoneNumber}',
                              style: Theme.of(context).textTheme.headline1?.copyWith(color: Colors.black,fontSize: 16,fontWeight: FontWeight.normal)
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          // Positioned(
          //     bottom: 30,
          //     right: 50,
          //     left: 50,
          //     child: buttonNext())
        ],
      ),
    );
  }

  Widget buttonNext(){
    return InkWell(
      onTap: (){
        goToMainScreen();
      },
      child: Container(
        height: 45,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: HexColor(COLOR_RED),
        ),
        child: Center(
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                text:'Next',
                style: Theme.of(context).textTheme.headline1?.copyWith(fontSize: 14,color: Colors.white)
            ),
          ),
        ),
      ),
    );
  }

  goToOTPScreen() async {
    if(mounted)
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (_){
          return OTPScreen();
        }),
      );
  }

  goToMainScreen() async {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (_){
        return RegisterScreen();
      }),
    );
  }
}
