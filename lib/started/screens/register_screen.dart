import 'dart:async';
import 'dart:developer';

import 'package:as2in1_app/const/colors_const.dart';
import 'package:as2in1_app/started/screens/otp_screen.dart';
import 'package:as2in1_app/started/screens/verifikasi_choose_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  double opacityLevel = 0.0;
  TextEditingController _phoneNumberController = TextEditingController();
  @override
  void initState() {
    super.initState();
    //startSplashScreen();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Icon(Icons.arrow_back_rounded,color: Colors.white,),
        actions: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Icon(Icons.help_rounded,color: Colors.grey,),
          )
        ],
      ),
      body: Container(
        child: ListView(
          padding: EdgeInsets.only(top: 0),
          physics: BouncingScrollPhysics(),
          children: [
            Stack(
              children: [
                Image.asset("assets/ic_slice_top.png",fit: BoxFit.cover,width: 250),

              ],
            ),
            Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(left: 50,right: 50),
                child:Center(
                  child: ListView(
                    physics: BouncingScrollPhysics(),
                    shrinkWrap: true,
                    children: [
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text:'Let\'s get you started',
                             ),
                      ),
                      SizedBox(height: 10,),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text:'Simply register your mobile number below',
                         ),
                      ),
                      SizedBox(height: 30,),
                      Container(
                        height: 45,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(40),
                              topRight: Radius.circular(40),
                              bottomLeft: Radius.circular(40),
                              bottomRight: Radius.circular(40)
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.9),
                              spreadRadius:1.5,
                              blurRadius: 1.5,
                              offset: Offset(1, 0), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Center(
                          child: widgetInputFeldApp(
                              field: "",
                              label: "",
                              controller: _phoneNumberController,
                              hint: "",
                              textInputType: TextInputType.number,
                              decoration: decoration(
                                  hint:"Input your phone number",
                                  label:"",
                                  asset:""
                              )
                          ),
                        ),
                        padding: EdgeInsets.only(
                            left: 20
                        ),
                      ),
                      SizedBox(height: 30,),
                      Image.asset("assets/ic_register_ava.png",height: 250,),
                      buttonNext(),
                      SizedBox(height: 20,),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text:'By Registering you are agreeing to \nour ',
                            children: [
                              TextSpan(
                                text:'terms of service ',
                              ),
                            ]
                        ),
                      ),
                    ],
                  ),
                )
            ),
          ],
        ),
      ),
    );
  }

  goToOTPScreen() async {
    if(mounted)
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (_){
        return OTPScreen();
      }),
    );
  }

  goToChooseVeririfacationOTPScreen() async {
    if(mounted)
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (_){
          return ChooseVerificastionScreenn(phoneNumber: _phoneNumberController.value.text,);
        }),
      );
  }

  Widget widgetInputFeldApp({required String label,required String field, required TextInputType textInputType,required String hint, required InputDecoration decoration,required TextEditingController controller}) {
    return TextFormField(
      autofocus: true,
      controller: controller,
      cursorColor: HexColor(COLOR_GREY),
      style: TextStyle(
          fontFamily: 'Poppins-Regular',
          fontSize: 14,
          fontWeight: FontWeight.w300),
      keyboardType: textInputType,
      // validator: (value) {
      //   return Validators().validateFieldGeneral(label: label,value: value);
      // },
      onChanged: (value){
        setState((){
          if(this._phoneNumberController.value.text.isEmpty){

          }
          else{
          }

        });
      },
      decoration: decoration,
    );
  }

  InputDecoration decoration({required String label,required String hint,required String asset}){
    return InputDecoration(
      labelText: label,
      labelStyle: TextStyle(
          fontFamily: 'Poppins-Regular',
          color: HexColor(COLOR_GREY),
          fontSize: 18,
          fontWeight: FontWeight.w300),
      hintText: hint,
      enabledBorder: InputBorder.none,
      focusedBorder: InputBorder.none,
      border: InputBorder.none,
      // prefixIcon: Padding(
      //     padding: EdgeInsets.only(right: 5, bottom: 0, top: 0),
      //     child: Image.asset(
      //       asset,
      //       scale: 4,
      //     )),
    );
  }

  Widget buttonNext(){
    return InkWell(
      onTap: (){
       goToChooseVeririfacationOTPScreen();
      },
      child: Container(
        height: 45,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: HexColor(COLOR_RED),
        ),
        child: Center(
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                text:'Register',
            ),
          ),
        ),
      ),
    );
  }
}
