
import 'package:flutter/cupertino.dart';

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    int colors = 0;
    hexColor = hexColor.replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    if (hexColor.length == 8) {
      colors = int.parse("0x$hexColor");
    }
    return colors;
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
//ALL COLOR DEFINED HERE
const COLOR_6B6B6B = "#6B6B6B";
const COLOR_C57979 = "#604D44";
const COLOR_RED_AWAL ="#C57979";
const COLOR_5F5F5F = "#5F5F5F";
const COLOR_B26A52 = "#B26A52";
const COLOR_EDE2E2 = "#EDE2E2";
const COLOR_C4C4C4 = "#C4C4C4";
const COLOR_FFEEAD = "#FFEEAD";
const COLOR_89D7A6 = "#89D7A6";
const COLOR_429F65 = "#429F65";
const COLOR_3B5998 = "#3B5998";
const COLOR_440E0E = "#440E0E";
const COLOR_ABABAB = "#ABABAB";
const COLOR_PRIMARY = "#805E55";
const COLOR_604D44  = "#604D44";
const COLOR_RED     = "#823527";
const COLOR_GREY    = "#e2e1e2";


