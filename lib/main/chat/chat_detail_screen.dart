import 'dart:io';

import 'package:as2in1_app/const/colors_const.dart';
import 'package:as2in1_app/main/chat/widget_chat_sms.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChatDetailScreen extends StatefulWidget {
  final ChatUsers chatUsers ;
  ChatDetailScreen({required this.chatUsers});

  @override
  _ChatDetailScreenState createState() => _ChatDetailScreenState();
}

class _ChatDetailScreenState extends State<ChatDetailScreen> {
  TextEditingController _textChat = TextEditingController();
  ScrollController scrollController = ScrollController();
  static File? _image ;
  //final picker = ImagePicker();

  List<Chat> listChats = [];

  @override
  void initState(){
    super.initState();
    listChats = [
      Chat(user: User(username: "Abu",id: "1"),message:"halo" ,chatId:1,type: "text",image: _image),
      Chat(user: User(username: "${widget.chatUsers.text}",id: "2"),message:"Halo bro" ,chatId:2,type: "text",image:_image),

    ];

    // listChats.add(
    //     Chat(chatId: listChats.length + 1, type: "image",user: User(username: "Abu",id: "1"),message: "Hi, apakah barang ini ready ?",image: null)
    // );
  }

  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,flexibleSpace: SafeArea(
        child: Container(
          padding: EdgeInsets.only(right: 16),
          child: Row(
            children: <Widget>[
              IconButton(
                onPressed: (){
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back,color: Colors.black,),
              ),
              SizedBox(width: 2,),
              CircleAvatar(
                backgroundImage: NetworkImage("https://randomuser.me/api/portraits/men/5.jpg"),
                maxRadius: 20,
              ),
              SizedBox(width: 12,),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(widget.chatUsers.text,style: TextStyle( fontSize: 16 ,fontWeight: FontWeight.w600),),
                    SizedBox(height: 6,),
                    Text("Online",style: TextStyle(color: Colors.grey.shade600, fontSize: 13),),
                  ],
                ),
              ),
              Icon(Icons.settings,color: Colors.black54,),
            ],
          ),
          ),
        ),
      ),
        body: Stack(
          children: <Widget>[
            listChat(),
            Align(
              alignment: Alignment.bottomLeft,
              child: footer(node: node)
            ),
          ],
        ),
    );
  }


  Widget chatMine({required Chat chat}){
    return chat.type == "image" ? Container(
      margin: EdgeInsets.only(top: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              chat.image == null as File ? Container(
                height: 250,
                width:250,
                constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * .6),
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color:HexColor("#F0F0F0"),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(25),
                    topLeft: Radius.circular(25),
                    bottomLeft: Radius.circular(0),
                    bottomRight: Radius.circular(0),
                  ),
                  image : DecorationImage(
                    image: NetworkImage(
                        "www.google.com"),
                    fit: BoxFit.cover,
                  ),
                ),
              ) :  Container(
                height: 250,
                width:250,
                constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * .6),
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color:HexColor("#F0F0F0"),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(25),
                    topLeft: Radius.circular(25),
                    bottomLeft: Radius.circular(0),
                    bottomRight: Radius.circular(0),
                  ),
                  image : DecorationImage(
                    image: FileImage(chat.image as File),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                width:250,
                padding: const EdgeInsets.only(bottom: 0,left: 16, right: 16.0,top: 16.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(0),
                    bottomLeft: Radius.circular(0),
                    bottomRight: Radius.circular(0),
                  ),
                  color:HexColor("#F0F0F0"),
                ),
                constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * .6),

                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                      text: '${chat.message}',
                      style:
                      Theme.of(context).textTheme.headline1?.copyWith(fontSize: 10,fontWeight: FontWeight.normal)),
                ),
              ),
              Container(
                width:250,
                height: 50,
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(0),
                    bottomLeft: Radius.circular(25),
                    bottomRight: Radius.circular(0),
                  ),
                  color:HexColor("#F0F0F0"),
                ),
                constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * .6),

                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Text(
                        "${DateTime.now().hour}:${DateTime.now().minute}",
                        style: Theme.of(context).textTheme.headline1?.copyWith(fontSize: 12,color: Colors.green),
                      ),
                    ),
                    Flexible(child: Image.asset("assets/images/ic_check.png",scale: 3,))
                  ],
                ),
              ),
            ],
          ),
          SizedBox(width: 15),
        ],
      ),
    ) :
    Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Container(
              margin: EdgeInsets.only(top: 15),
              constraints: BoxConstraints(
                  minHeight: 80,
                  minWidth: 100,
                  maxWidth: MediaQuery.of(context).size.width * .6),

              padding: const EdgeInsets.all(16),
              decoration: BoxDecoration(
                color:HexColor("#F0F0F0"),
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(25),
                  topLeft: Radius.circular(25),
                  bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(0),
                ),
              ),
              child: Stack(
                children: [
                  RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                        text: '${chat.message}',
                        style:
                        Theme.of(context).textTheme.headline1?.copyWith(fontSize: 10,fontWeight: FontWeight.normal)),
                  ),
                  Positioned(
                    bottom: 0,
                    right: 0  ,
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: Text(
                        "${DateTime.now().hour}:${DateTime.now().minute}",
                        style: Theme.of(context).textTheme.headline1?.copyWith(fontSize: 10,color: Colors.green),
                      ),),
                  ),
                ],
              ),
            ),
          ],
        ),
        SizedBox(width: 16.0,)
      ],
    );
  }


  Widget chatClient({required Chat chat}){
    return chat.type == "image" ? Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //circleAvatarWidget(),
          SizedBox(width: 10,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Tasya",
                style: Theme.of(context).textTheme.caption,
              ),
              chat.type == "text" ? Container(
                constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * .6),
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color:HexColor("#F0F0F0"),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(25),
                    bottomLeft: Radius.circular(25),
                    bottomRight: Radius.circular(25),
                  ),
                ),
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                      text: '${chat.message}',
                      style:
                      Theme.of(context).textTheme.headline1?.copyWith(fontSize: 10,fontWeight: FontWeight.normal)),
                ),
              ) :
              Container(
                height: 250,
                width:250,
                constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * .6),
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color:HexColor("#F0F0F0"),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(25),
                    bottomLeft: Radius.circular(0),
                    bottomRight: Radius.circular(0),
                  ),
                  image : DecorationImage(
                    image: NetworkImage(
                        "https://lh3.googleusercontent.com/ogw/ADGmqu_LsjlGo-F9Jky5e64xLLPLEhSTtJ7m-dDVkJETcQ=s192-c-mo"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                width:250,
                padding: const EdgeInsets.only(bottom: 0,left: 16, right: 16.0,top: 16.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(0),
                    bottomLeft: Radius.circular(0),
                    bottomRight: Radius.circular(0),
                  ),
                  color:HexColor("#F0F0F0"),
                ),
                constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * .6),

                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                      text: '${chat.message}',
                      style:
                      Theme.of(context).textTheme.headline1?.copyWith(fontSize: 10,fontWeight: FontWeight.normal)),
                ),
              ),
              Container(
                width:250,
                height: 50,
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(0),
                    bottomLeft: Radius.circular(25),
                    bottomRight: Radius.circular(25),
                  ),
                  color:HexColor("#F0F0F0"),
                ),
                constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * .6),

                child: Row(
                  children: [
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Text(
                        "${DateTime.now().hour}:${DateTime.now().minute}",
                      ),
                    ),
                    Flexible(child: Image.asset("assets/images/ic_check.png"))
                  ],
                ),
              ),

            ],
          ),
          SizedBox(width: 15),
        ],
      ),
    ) :
    Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        //circleAvatarWidget(),
        SizedBox(width: 10,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(width: 15),
            Text(
              "${chat.user.username}",
              style: Theme.of(context).textTheme.caption,
            ),
            Container(
              constraints: BoxConstraints(
                  minHeight: 80,
                  minWidth: 100,
                  maxWidth: MediaQuery.of(context).size.width * .6),

              padding: const EdgeInsets.all(16),
              decoration: BoxDecoration(
                color:HexColor("#F0F0F0"),
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(25),
                  topLeft: Radius.circular(0),
                  bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25),
                ),
              ),
              child: Stack(
                children: [
                  RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                        text: '${chat.message}',
                        style:
                        Theme.of(context).textTheme.headline1?.copyWith(fontSize: 10,fontWeight: FontWeight.normal)),
                  ),
                  Positioned(
                    bottom: 0,
                    right: 0,
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: Text(
                        "${DateTime.now().hour}:${DateTime.now().minute}",
                        style: Theme.of(context).textTheme.headline1?.copyWith(fontSize: 12,color: Colors.green),
                      ),),
                  ),

                  // Positioned(
                  //   bottom: 5,
                  //   right: 0,
                  //   child: Align(
                  //       alignment: Alignment.bottomRight,
                  //       child:Image.asset("assets/images/ic_check.png",scale: 2.5,)),
                  // ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget listChat(){
    return ListView.builder(
      padding: EdgeInsets.only(bottom: 100),
        controller:scrollController ,
        itemCount:listChats.length,
        shrinkWrap: true,
        itemBuilder:(BuildContext context, int index){
          Chat chat = listChats[index];

          return chat.user.id == "1" ? chatMine(chat: chat) : chatClient(chat:chat) ;
        });
  }

  Widget footer({required FocusScopeNode node}){
    return Container(
      padding: EdgeInsets.all(16.0),
      color: Colors.white,
      child: Row(
        children: [
          // InkWell(
          //     onTap: () async {
          //       await getImage();
          //       // setState(() {
          //       //   listChats.add(
          //       //       Chat(message: _textChat.value.text,user: User(id: "1",username: "Abu"),chatId: listChats.length + 1,type: "text"),
          //       //   );
          //       //   scrollController.jumpTo(MediaQuery.of(context).size.height);
          //       //   node.unfocus();
          //       //   _textChat.clear();
          //       // });
          //     },
          //     child: Image.asset("assets/images/ic_camera.png",scale: 4,)),
          SizedBox(width: 10,),
          Flexible(
            child: TextFormField(
              controller: _textChat,
              autofocus: false,
              maxLines: null,
              style: TextStyle(fontSize: 15.0, color: Colors.black),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: HexColor("#E6E6E6"),style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(30.0),
                ),
                hintText: 'type something',
                filled: true,
                fillColor: HexColor("#FBFBFB"),
                enabled: true,
                contentPadding: const EdgeInsets.only(
                    left: 14.0, bottom: 6.0, top: 8.0),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color:  HexColor("#E6E6E6"),style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(30.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: HexColor("#E6E6E6"),style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(30.0),
                ),
                disabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: HexColor("#E6E6E6"),style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(30.0),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: HexColor("#E6E6E6"),style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(30.0),
                ),
              ),
            ),
          ),
          SizedBox(width: 10,),
          InkWell(
              onTap: () async {
                //await getImage();
                setState(() {
                  listChats.add(
                    // ignore: unnecessary_null_comparison
                    _image == null  ?
                    Chat(message: _textChat.value.text,user: User(id: "1",username: "Abu"),chatId: listChats.length + 1,type: "text",image: _image)
                        :  Chat(message: _textChat.value.text,user: User(id: "1",username: "Abu"),chatId: listChats.length + 1,type: "image",image: _image),
                  );
                  //scrollController.jumpTo(MediaQuery.of(context).size.height);
                  node.unfocus();
                  _textChat.clear();
                  _image?.delete();
                });
              },
              child: Icon(Icons.send,color: Colors.blue,size: 30,)),
          SizedBox(width: 20,),
        ],
      ),
    );
  }
}

class Chat{
  String type;
  int chatId;
  User user;
  String message;
  File? image;

  Chat({required this.chatId, required this.user, required this.message,required this.type,required this.image});
}
class User {
  String id;
  String username;
  User({required this.id, required this.username});
}
