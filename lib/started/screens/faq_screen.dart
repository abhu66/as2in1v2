
import 'package:as2in1_app/const/colors_const.dart';
import 'package:as2in1_app/started/screens/register_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FAQScreen extends StatefulWidget {
  @override
  _FAQScreenState createState() => _FAQScreenState();
}

class _FAQScreenState extends State<FAQScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.6,
        centerTitle: false,
        title: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
              text:'FAQ',
              style: Theme.of(context).textTheme.headline1?.copyWith(color: Colors.black,fontSize: 18)
          ),
        ),
        actions: [
          InkWell(
            onTap: (){

            },
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Image.asset("assets/ic_faq.png",color: HexColor(COLOR_RED),width: 35,height: 35,),
            ),
          )
        ],
      ),
      body: Stack(
        children: [
          Container(
            child: ListView(
              padding: EdgeInsets.only(right: 30),
              physics: BouncingScrollPhysics(),
              children: [
                SizedBox(height: 16,),
                ListTile(
                  title: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                        text:'Why do i need to register?',
                        style: Theme.of(context).textTheme.headline1?.copyWith(color: Colors.black,fontSize: 18)
                    ),
                  ),
                  subtitle: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                        text:'\nTo be able to use the As2in1 functionalities, we need to register this app to a mobile',
                        style: Theme.of(context).textTheme.headline1?.copyWith(color: Colors.black,fontSize: 16,fontWeight: FontWeight.normal)
                    ),
                  ),
                ),

                SizedBox(height: 16,),
                ListTile(
                  title: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                        text:'Why do you need my number?',
                        style: Theme.of(context).textTheme.headline1?.copyWith(color: Colors.black,fontSize: 18)
                    ),
                  ),
                  subtitle: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                        text:'\nWe need to send OTP (one time password) to your mobile number to ensure your As2in1 is secure.'
                            'You will only need to receive the OTP once and you\'ll be ready to go',
                        style: Theme.of(context).textTheme.headline1?.copyWith(color: Colors.black,fontSize: 16,fontWeight: FontWeight.normal)
                    ),
                  ),
                ),

                SizedBox(height: 16,),
                ListTile(
                  title: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                        text:'What\'s an OTP?',
                        style: Theme.of(context).textTheme.headline1?.copyWith(color: Colors.black,fontSize: 18)
                    ),
                  ),
                  subtitle: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                        text:'\nAn OTP is one-time password, sent via SMS to your mobile device.',
                        style: Theme.of(context).textTheme.headline1?.copyWith(color: Colors.black,fontSize: 16,fontWeight: FontWeight.normal)
                    ),
                  ),
                ),

                SizedBox(height: 16,),
                ListTile(
                  title: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                        text:'Need to speak to support?',
                        style: Theme.of(context).textTheme.headline1?.copyWith(color: Colors.black,fontSize: 18)
                    ),
                  ),
                  subtitle: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                        text:'\nSure! You can speak to our customer service agent by clicking the chat icon.',
                        style: Theme.of(context).textTheme.headline1?.copyWith(color: Colors.black,fontSize: 16,fontWeight: FontWeight.normal)
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
              bottom: 30,
              right: 50,
              left: 50,
              child: buttonNext())
        ],
      ),
    );
  }

  Widget buttonNext(){
    return InkWell(
      onTap: (){
        goToMainScreen();
      },
      child: Container(
        height: 45,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: HexColor(COLOR_RED),
        ),
        child: Center(
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                text:'Next',
                style: Theme.of(context).textTheme.headline1?.copyWith(fontSize: 14,color: Colors.white)
            ),
          ),
        ),
      ),
    );
  }

  goToMainScreen() async {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (_){
        return RegisterScreen();
      }),
    );
  }
}
