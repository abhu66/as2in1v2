import 'dart:math';

import 'package:as2in1_app/const/colors_const.dart';
import 'package:as2in1_app/main/events/screens/upcoming_events_screens.dart';
import 'package:as2in1_app/main/home/widget_bottom_sheet_produk_digital.dart';
import 'package:as2in1_app/main/home/widget_bottom_sheet_purchase.dart';
import 'package:as2in1_app/main/home/widget_bottom_sheet_service.dart';
import 'package:as2in1_app/main/home/widget_bottom_sheet_topup.dart';
import 'package:as2in1_app/main/home/widget_bottom_sheet_voiceandsms.dart';
import 'package:as2in1_app/main/home/widget_recomendation.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int currentIndex = 0;
  // Used to generate random integers
  final _random = Random();

  List cardList = [Item1(), Item1(), Item1(), Item1()];

  List<MenuItem> listMenuPurchase = [];

  List<MyPackage> listMyPackage = [];

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }




  @override
  Widget build(BuildContext context) {
    listMyPackage    = [
      MyPackage(
        icon: "assets/data.png",
        title: ""),
      MyPackage(
          icon: "assets/sms.png",
          title: ""),
      MyPackage(icon: "assets/telepon.png", title: ""),
    ];

    listMenuPurchase = [
      MenuItem(
          icon: "assets/purchase/balance_transfer_to_id.png",
          title: "Balance Transfer to Indonesia"),
      MenuItem(
          icon: "assets/purchase/balance_transfer_to_id.png",
          title: "Balance Transfer to Malaysia"),
      MenuItem(icon: "assets/purchase/pln_prepaid.png", title: "PLN Prepaid"),
      MenuItem(
          icon: "assets/purchase/data_transfer_to_indonesia.png",
          title: "Data Transfer to Indonesia"),
      MenuItem(
          icon: "assets/purchase/alfamart_voucher.png",
          title: "Alfamart Voucher"),
      MenuItem(
          icon: "assets/purchase/indomaret.png", title: "Indomaret Voucher"),
      MenuItem(
          icon: "assets/purchase/wifi_indonesia.png", title: "Wifi Indonesia"),
      MenuItem(
          icon: "assets/purchase/crbt.png", title: "Caller Ring Back Tone"),
    ];
    return Scaffold(
      backgroundColor: HexColor("#F0F8FF"),
      extendBodyBehindAppBar: false,
      appBar: AppBar(
        centerTitle: false,
        elevation: 0.0,
        leadingWidth: 0.0,
        toolbarHeight: 80,
        title:  widgetBalance(),
        // title: Row(
        //   children: [
        //     Text("As2in1"),
        //     SizedBox(
        //       width: 10,
        //     ),
        //     Icon(
        //       Icons.arrow_forward_rounded,
        //       size: 18,
        //     ),
        //     SizedBox(
        //       width: 10,
        //     ),
        //     Text(
        //       "+6281218209581",
        //       style: TextStyle(fontSize: 18),
        //     ),
        //   ],
        // ),
        backgroundColor: Colors.transparent,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                HexColor("#891F1F"),
                HexColor("#B11B33"),
              ],
            ),
          ),
        ),
        actions: [
          IconButton(onPressed: () {

          }, icon: Icon(Icons.notifications_rounded))
        ],
      ),
      body: Container(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(bottom: 100),
          children: [
            Stack(
              children: [
                Container(
                  height: 220,
                  decoration: BoxDecoration(
                    // borderRadius: BorderRadius.only(
                    //   bottomRight: Radius.circular(30),
                    //   bottomLeft: Radius.circular(30),
                    // ),
                    gradient: LinearGradient(
                      colors: [
                        HexColor("#891F1F"),
                        HexColor("#B11B33"),
                      ],
                    ),
                  ),
                  child: Image.asset(
                    "assets/icons/bg.png",
                    width: MediaQuery.of(context).size.width,
                    scale: 5,
                  ),
                ),
                // Container(
                //     child: ListTile(
                //   title: RichText(
                //     textAlign: TextAlign.left,
                //     text: TextSpan(
                //         text: "\$",
                //         style: TextStyle(
                //             color: Colors.white,
                //             fontSize: 20,
                //             fontWeight: FontWeight.bold),
                //         children: [
                //           TextSpan(
                //               text: "20.000",
                //               style: TextStyle(
                //                   color: Colors.white,
                //                   fontSize: 30,
                //                   fontWeight: FontWeight.bold)),
                //         ]),
                //   ),
                //   subtitle: RichText(
                //     textAlign: TextAlign.left,
                //     text: TextSpan(
                //       text: "My Balance",
                //       style: TextStyle(
                //           color: Colors.white,
                //           fontSize: 18,
                //           fontWeight: FontWeight.bold),
                //     ),
                //   ),
                // )),
                widgetPromo(),
              ],
            ),
            // SizedBox(height: 10,),
            // Container(
            //   height: 150,
            //   margin: EdgeInsets.only(left: 0,right: 0),
            //   decoration: BoxDecoration(
            //       color: Colors.white,
            //       borderRadius: BorderRadius.circular(10),
            //   ),
            //   child: ListView(
            //     shrinkWrap: true,
            //     physics: NeverScrollableScrollPhysics(),
            //     children : [
            //       Padding(
            //         padding: const EdgeInsets.all(8.0),
            //         child: Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [
            //             RichText(
            //               textAlign: TextAlign.left,
            //               text: TextSpan(
            //                 text: "My Package",
            //                 style: TextStyle(
            //                     color: Colors.black,
            //                     fontSize: 18,
            //                     fontWeight: FontWeight.w700),
            //               ),
            //             ),
            //
            //             RichText(
            //               textAlign: TextAlign.left,
            //               text: TextSpan(
            //                 text: "See My Package",
            //                 style: TextStyle(
            //                     color: HexColor("#0038FF"),
            //                     fontSize: 14,
            //                     fontWeight: FontWeight.w600),
            //               ),
            //             ),
            //           ],
            //         ),
            //       ),
            //       Container(
            //         padding: EdgeInsets.all(10),
            //         child: Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceAround,
            //           children: [
            //             Column(
            //               mainAxisAlignment:MainAxisAlignment.center,
            //               children: [
            //                 Image.asset("assets/icons/topup.png",scale: 7,),
            //                 RichText(
            //                   textAlign: TextAlign.center,
            //                   text: TextSpan(
            //                     text: "Top Up",
            //                     style: TextStyle(
            //                         color: Colors.black,
            //                         fontSize: 14,
            //                         fontWeight: FontWeight.normal),
            //                   ),
            //                 ),
            //               ],
            //             ),
            //             Column(
            //               mainAxisAlignment:MainAxisAlignment.center,
            //               children: [
            //                 Image.asset("assets/icons/purchase.png",scale: 7,),
            //                 RichText(
            //                   textAlign: TextAlign.center,
            //                   text: TextSpan(
            //                     text: "Purchase",
            //                     style: TextStyle(
            //                         color: Colors.black,
            //                         fontSize: 14,
            //                         fontWeight: FontWeight.normal),
            //                   ),
            //                 ),
            //               ],
            //             ),
            //             Column(
            //               mainAxisAlignment:MainAxisAlignment.center,
            //               children: [
            //                 Image.asset("assets/icons/voiceandsms.png",scale: 7,),
            //                 RichText(
            //                   textAlign: TextAlign.center,
            //                   text: TextSpan(
            //                     text: "Voice & SMS",
            //                     style: TextStyle(
            //                         color: Colors.black,
            //                         fontSize: 14,
            //                         fontWeight: FontWeight.normal),
            //                   ),
            //                 ),
            //               ],
            //             ),
            //           ],
            //         ),
            //       ),
            //     ]
            //   ),
            // ),
            widgetMyPakcge(),
            SizedBox(
              height: 10,
            ),
            widgetPurchase(),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 150,
              margin: EdgeInsets.only(left: 0, right: 0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        RichText(
                          textAlign: TextAlign.left,
                          text: TextSpan(
                            text: "Produk Digital",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                        InkWell(
                          onTap: (){
                            showPordukDigitalAll();
                          },
                          child: RichText(
                            textAlign: TextAlign.left,
                            text: TextSpan(
                              text: "Lihat Semua",
                              style: TextStyle(
                                  color: HexColor("#0038FF"),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                      height: 250,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      margin: EdgeInsets.only(top: 10),
                      child: ListView.builder(
                          itemCount: choicesDigital.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              width: 150,
                              child: ListView(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                children: [
                                  ClipOval(
                                    child: Center(
                                      child: Container(
                                          width: 50,
                                          height: 50,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(40),
                                            color: HexColor("#F0F8FF"),
                                          ),
                                          child: Center(
                                              child: Image.asset(
                                            "${choicesDigital[index].icon}",
                                            width: 30,
                                            height: 30,
                                          ))),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Flexible(
                                        child: RichText(
                                          textAlign: TextAlign.center,
                                          text: TextSpan(
                                            text:
                                                "${choicesDigital[index].title}",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 14,
                                                fontWeight: FontWeight.normal),
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            );
                          })),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            widgetService(),
            SizedBox(
              height: 10,
            ),
            widgetVoiceAndSms(),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 180,
              margin: EdgeInsets.only(left: 0, right: 0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        RichText(
                          textAlign: TextAlign.left,
                          text: TextSpan(
                            text: "Rekomendasi",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                        RichText(
                          textAlign: TextAlign.left,
                          text: TextSpan(
                            text: "Lihat Semua",
                            style: TextStyle(
                                color: HexColor("#0038FF"),
                                fontSize: 14,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20)),
                    margin: EdgeInsets.only(top: 10),
                    child: CarouselSlider(
                      options: CarouselOptions(
                        height: 100,
                        autoPlay: true,
                        autoPlayInterval: Duration(seconds: 3),
                        autoPlayAnimationDuration: Duration(milliseconds: 800),
                        autoPlayCurve: Curves.fastOutSlowIn,
                        pauseAutoPlayOnTouch: true,
                        aspectRatio: 2.0,
                        onPageChanged: (index, reason) {
                          setState(() {
                            currentIndex = index;
                          });
                        },
                      ),
                      items: cardList.map((card) {
                        return Builder(builder: (BuildContext context) {
                          return Container(
                            margin: EdgeInsets.only(right: 10),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: AssetImage('assets/icons/slide.png')),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            height: MediaQuery.of(context).size.height * 0.30,
                            width: MediaQuery.of(context).size.width,
                          );
                        });
                      }).toList(),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5, left: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: map<Widget>(cardList, (index, url) {
                        return Container(
                          width: currentIndex == index ? 20.0 : 10.0,
                          height: 8.0,
                          margin: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 2.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: currentIndex == index
                                ? Colors.red
                                : Colors.grey,
                          ),
                        );
                      }),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10
            ),
            widgetEvent(),
            SizedBox(
                height: 10
            ),
            widgetCustomerService()

          ],
        ),
      ),
    );
  }

  Widget widgetGridMenuPurchase() {
    return ListView.builder(
        padding: EdgeInsets.only(top: 10),
        itemCount: listMenuPurchase.length,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            width: 100,
            child: ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: [
                ClipOval(
                  child: Center(
                    child: Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          color: HexColor("#F0F8FF"),
                        ),
                        child: Center(
                            child: Image.asset(
                          "${listMenuPurchase[index].icon}",
                          width: 30,
                          height: 30,
                        ))),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: "${listMenuPurchase[index].title}",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 10,
                              fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
        });
  }

  Widget widgetPurchase() {
    return Container(
      height: 150,
      margin: EdgeInsets.only(left: 0, right: 0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: "Pembeliann",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                InkWell(
                  onTap: (){
                    showPurchaseAll();
                  },
                  child: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      text: "Lihat Semua",
                      style: TextStyle(
                          color: HexColor("#0038FF"),
                          fontSize: 14,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
              height: 250,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              margin: EdgeInsets.only(top: 10),
              child: ListView.builder(
                  itemCount: choicesPurchase.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      width: 150,
                      child: ListView(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        children: [
                          ClipOval(
                            child: Center(
                              child: Container(
                                  width: 50,
                                  height: 50,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(40),
                                    color: HexColor("#F0F8FF"),
                                  ),
                                  child: Center(
                                      child: Image.asset(
                                        "${choicesPurchase[index].icon}",
                                        width: 30,
                                        height: 30,
                                      ))),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                child: RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(
                                    text: "${choicesPurchase[index].title}",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.normal),
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    );
                  })),
        ],
      ),
    );
  }

  Widget widgetVoiceAndSms() {
    return Container(
      height: 150,
      margin: EdgeInsets.only(left: 0, right: 0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: "Voice & SMS",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                InkWell(
                  onTap: (){
                    showSerivceAll();
                  },
                  child: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      text: "Lihat Semua",
                      style: TextStyle(
                          color: HexColor("#0038FF"),
                          fontSize: 14,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
              height: 250,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              margin: EdgeInsets.only(top: 10),
              child: ListView.builder(
                  itemCount: choicesVoiceAndSms.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      width: 150,
                      child: ListView(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        children: [
                          ClipOval(
                            child: Center(
                              child: Container(
                                  width: 50,
                                  height: 50,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(40),
                                    color: HexColor("#F0F8FF"),
                                  ),
                                  child: Center(
                                      child: Image.asset(
                                        "${choicesVoiceAndSms[index].icon}",
                                        width: 30,
                                        height: 30,
                                      ))),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                child: RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(
                                    text: "${choicesVoiceAndSms[index].title}",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.normal),
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    );
                  })),
        ],
      ),
    );
  }

  Widget widgetService() {
    return Container(
      height: 150,
      margin: EdgeInsets.only(left: 0, right: 0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: "Layanan",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                InkWell(
                  onTap: (){
                    showSerivceAll();
                  },
                  child: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      text: "Lihat Semua",
                      style: TextStyle(
                          color: HexColor("#0038FF"),
                          fontSize: 14,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
              height: 250,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              margin: EdgeInsets.only(top: 10),
              child: ListView.builder(
                  itemCount: choicesServices.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      width: 150,
                      child: ListView(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        children: [
                          ClipOval(
                            child: Center(
                              child: Container(
                                  width: 50,
                                  height: 50,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(40),
                                    color: HexColor("#F0F8FF"),
                                  ),
                                  child: Center(
                                      child: Image.asset(
                                    "${choicesServices[index].icon}",
                                    width: 30,
                                    height: 30,
                                  ))),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                child: RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(
                                    text: "${choicesServices[index].title}",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.normal),
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    );
                  })),
        ],
      ),
    );
  }

  Widget widgetMyPakcge() {
    return Container(
      height: 140,
      margin: EdgeInsets.only(left: 0, right: 0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: "Paket Saya",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                InkWell(
                  onTap: (){
                    // Navigator.of(context).push(
                    //   MaterialPageRoute(builder: (_){
                    //     return UpcomingEventsScreen();
                    //   }),
                    // );
                  },
                  child: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      text: "Lihat Semua",
                      style: TextStyle(
                          color: HexColor("#0038FF"),
                          fontSize: 14,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
              height: 280,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              margin: EdgeInsets.only(top: 6),
              child: ListView.builder(
                  itemCount: 3,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return
                    Container(
                      width: 100,
                      height: 100,
                      child: ListView(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        children: [
                          Center(
                            child: Center(
                                child: Image.asset(
                                  "${listMyPackage[index].icon}",
                                  width: 80,
                                  height:90,
                                )),
                          ),
                        ],
                      ),
                    );
                  })),
        ],
      ),
    );
  }

  Widget widgetCustomerService() {
    return Container(
      height: 250,
      margin: EdgeInsets.only(left: 0, right: 0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: "Customer Service",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w700),
                  ),
                ),
              ],
            ),
          ),
          Container(
              height: 280,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              margin: EdgeInsets.only(top: 6),
              child: ListView(
                  scrollDirection: Axis.horizontal,
                  children : [
                      Container(
                        width: 150,
                        height: 150,
                        margin: EdgeInsets.only(left: 10),
                        child: ListView(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          children: [
                            Container(
                              height: 180,
                              padding: EdgeInsets.all(10),

                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset("assets/veronika.png",width: 80,height: 80,),
                                  SizedBox(height: 10,),
                                  Row(
                                    children: [
                                      RichText(
                                        textAlign: TextAlign.left,
                                        text: TextSpan(
                                          text: "Ask Veronika",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                    ],
                                  ),
                                  RichText(
                                    textAlign: TextAlign.left,
                                    text: TextSpan(
                                      text: "Any question ? Chat with us!",
                                      style: TextStyle(
                                          color: Colors.black45,
                                          fontSize: 12,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ],
                              ),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                  border: Border.all(
                                      color: Colors.grey
                                  )
                              ),
                            ),
                          ],
                        ),
                      ),


                    Container(
                      width: 150,
                      height: 150,
                      margin: EdgeInsets.only(left: 10),
                      child: ListView(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        children: [
                          Container(
                            height: 180,
                            padding: EdgeInsets.all(10),

                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset("assets/info.png",width: 80,height: 80,),
                                SizedBox(height: 10,),
                                Row(
                                  children: [
                                    RichText(
                                      textAlign: TextAlign.left,
                                      text: TextSpan(
                                        text: "Get Help",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                  ],
                                ),
                                RichText(
                                  textAlign: TextAlign.left,
                                  text: TextSpan(
                                    text: "Contact us to get info",
                                    style: TextStyle(
                                        color: Colors.black45,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                                border: Border.all(
                                    color: Colors.grey
                                )
                            ),
                          ),
                        ],
                      ),
                    ),


                    Container(
                      width: 150,
                      height: 150,
                      margin: EdgeInsets.only(left: 10),
                      child: ListView(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        children: [
                          Container(
                            height: 180,
                            padding: EdgeInsets.all(10),

                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset("assets/ic_faq.png",width: 80,height: 80,color: HexColor(COLOR_RED),),
                                SizedBox(height: 10,),
                                Row(
                                  children: [
                                    RichText(
                                      textAlign: TextAlign.left,
                                      text: TextSpan(
                                        text: "FAQ",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                  ],
                                ),
                                RichText(
                                  textAlign: TextAlign.left,
                                  text: TextSpan(
                                    text: "Quick & useful Q&A",
                                    style: TextStyle(
                                        color: Colors.black45,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                                border: Border.all(
                                    color: Colors.grey
                                )
                            ),
                          ),
                        ],
                      ),
                    ),
                    ],
                  )
            ),
        ],
      ),
    );
  }

  Widget widgetPromo() {
    return Container(
      height: 180,
      margin: EdgeInsets.only(left: 0, right: 0, top: 50),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Container(
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
            margin: EdgeInsets.only(top: 10),
            child: CarouselSlider(
              options: CarouselOptions(
                height: 120,
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 3),
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                autoPlayCurve: Curves.fastOutSlowIn,
                pauseAutoPlayOnTouch: true,
                aspectRatio: 2.0,
                onPageChanged: (index, reason) {
                  setState(() {
                    currentIndex = index;
                  });
                },
              ),
              items: cardList.map((card) {
                return Builder(builder: (BuildContext context) {
                  return Container(
                    margin: EdgeInsets.only(right: 10),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage('assets/icons/slide.png')),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    height: MediaQuery.of(context).size.height * 0.30,
                    width: MediaQuery.of(context).size.width,
                  );
                });
              }).toList(),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 0, left: 20,bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: map<Widget>(cardList, (index, url) {
                return Container(
                  width: currentIndex == index ? 20.0 : 10.0,
                  height: 8.0,
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: currentIndex == index ? Colors.red : Colors.grey,
                  ),
                );
              }),
            ),
          ),
        ],
      ),
    );
  }

  Widget widgetEvent() {
    return Container(
      height: 180,
      margin: EdgeInsets.only(left: 0, right: 0, top: 0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    text: "Upcoming Events",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                InkWell(
                  onTap: (){
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (_){
                        return UpcomingEventsScreen();
                      }),
                    );
                  },
                  child: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      text: "View All",
                      style: TextStyle(
                          color: HexColor("#0038FF"),
                          fontSize: 14,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
            margin: EdgeInsets.only(top: 10, bottom: 10),
            child: CarouselSlider(
              options: CarouselOptions(
                height: 80,
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 3),
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                autoPlayCurve: Curves.fastOutSlowIn,
                pauseAutoPlayOnTouch: true,
                aspectRatio: 2.0,
                onPageChanged: (index, reason) {
                  setState(() {
                    currentIndex = index;
                  });
                },
              ),
              items: cardList.map((card) {
                return Builder(builder: (BuildContext context) {
                  return Container(
                    margin: EdgeInsets.only(right: 10),
                    decoration: BoxDecoration(
                      color: HexColor("#F0F8FF"),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: ListTile(
                        dense: true,
                        leading: Container(
                          height: 30,
                          width: 80,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.primaries[
                                    _random.nextInt(Colors.primaries.length)]
                                [_random.nextInt(9) * 100],
                          ),
                          child: Center(
                            child: RichText(
                              textAlign: TextAlign.left,
                              text: TextSpan(
                                text: "AUG 10",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                          ),
                        ),
                        title: RichText(
                          textAlign: TextAlign.left,
                          text: TextSpan(
                            text: "Volunteer Board Meeting".toUpperCase(),
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                        subtitle: RichText(
                          textAlign: TextAlign.left,
                          text: TextSpan(
                            text: "LOREM IPSUM DOLOR SIT AMET CONSECTETUR",
                            style: TextStyle(
                                color: Colors.grey,
                                fontSize: 10,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ),
                    ),
                  );
                });
              }).toList(),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 5, left: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: map<Widget>(cardList, (index, url) {
                return Container(
                  width: currentIndex == index ? 20.0 : 10.0,
                  height: 8.0,
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: currentIndex == index ? Colors.red : Colors.grey,
                  ),
                );
              }),
            ),
          ),
        ],
      ),
    );
  }

  Widget widgetBalance() {
    return Container(
      margin: EdgeInsets.only(left: 0, right: 0,top: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),),
      child: ListTile(
        leading:ClipOval(
          child: ClipOval(
            child: Image.asset(
              "assets/ic_as_red.png",
              width: 40,
              height: 40,
            ),
          ),
        ),
        title: RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
              text: "\$",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text: "20.000",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold)),
                TextSpan(
                    text: " USD",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                        fontWeight: FontWeight.bold)),
              ]),
        ),
        subtitle: Row(
          children: [
            RichText(
              textAlign: TextAlign.left,
              text: TextSpan(
                text: "Active until 15 Aug 2021",
                style: TextStyle(
                    color: Colors.white, fontSize: 12, fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(width: 10,),
            InkWell(
              onTap: (){
                showToUp();
              },
              child: Container(
                padding: EdgeInsets.all(5),
                  height: 30,
                  width: 80,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    gradient: LinearGradient(
                      colors: [
                        HexColor("#891F1F"),
                        HexColor("#B11B33"),
                      ],
                    ),
                  ),
                  child: Center(
                      child: RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(
                          text: "Top Up",
                          style: TextStyle(
                              color: Colors.white, fontSize: 12, fontWeight: FontWeight.bold),
                        ),
                      ),
                  )),
            ),
          ],
        ),
      ),
    );
  }


  showPurchaseAll(){
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30),
              topLeft: Radius.circular(30)
          ),
        ),
        backgroundColor: Colors.white,
        isScrollControlled: true,
        context: context, builder: (BuildContext context){
      return Container(
          height: MediaQuery.of(context).size.height - AppBar().preferredSize.height,
          child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              InkWell(
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text:'Purchase Option',
                            style:TextStyle(fontSize: 18.0,color: Colors.black)
                        ),
                      ),
                      Icon(Icons.keyboard_arrow_down_rounded,size: 35,)
                    ],
                  ),
                  margin: EdgeInsets.all(20),
                ),
                onTap: (){
                  Navigator.of(context).pop();
                },
              ),
              WidgetBottomSheetPurchase()
            ],
          ));
    }
    );
  }


  showPordukDigitalAll(){
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30),
              topLeft: Radius.circular(30)
          ),
        ),
        backgroundColor: Colors.white,
        isScrollControlled: true,
        context: context, builder: (BuildContext context){
      return Container(
          height: MediaQuery.of(context).size.height - AppBar().preferredSize.height,
          child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              InkWell(
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text:'Product Digital Option',
                            style:TextStyle(fontSize: 18.0,color: Colors.black)
                        ),
                      ),
                      Icon(Icons.keyboard_arrow_down_rounded,size: 35,)
                    ],
                  ),
                  margin: EdgeInsets.all(20),
                ),
                onTap: (){
                  Navigator.of(context).pop();
                },
              ),
              WidgetBottomSheetProdukDigital()
            ],
          ));
    }
    );
  }

  showToUp(){
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30),
              topLeft: Radius.circular(30)
          ),
        ),
        backgroundColor: Colors.white,
        isScrollControlled: true,
        context: context, builder: (BuildContext context){
      return Container(
          height: MediaQuery.of(context).size.height - AppBar().preferredSize.height,
          child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              InkWell(
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text:'Top Up Option',
                            style:TextStyle(fontSize: 18.0,color: Colors.black)
                        ),
                      ),
                      Icon(Icons.keyboard_arrow_down_rounded,size: 35,)
                    ],
                  ),
                  margin: EdgeInsets.all(20),
                ),
                onTap: (){
                  Navigator.of(context).pop();
                },
              ),
              WidgetBottomSheetTopUp()
            ],
          ));
    }
    );
  }


  showSerivceAll(){
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30),
              topLeft: Radius.circular(30)
          ),
        ),
        backgroundColor: Colors.white,
        isScrollControlled: true,
        context: context, builder: (BuildContext context){
      return Container(
          height: MediaQuery.of(context).size.height - AppBar().preferredSize.height,
          child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              InkWell(
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text:'Services Option',
                            style:TextStyle(fontSize: 18.0,color: Colors.black)
                        ),
                      ),
                      Icon(Icons.keyboard_arrow_down_rounded,size: 35,)
                    ],
                  ),
                  margin: EdgeInsets.all(20),
                ),
                onTap: (){
                  Navigator.of(context).pop();
                },
              ),
              WidgetBottomSheetService()
            ],
          ));
    }
    );
  }
}

class MenuItem {
  String title;
  String icon;

  MenuItem({required this.icon, required this.title});
}

class MyPackage {
  String title;
  String icon;

  MyPackage({required this.icon, required this.title});
}
