
import 'dart:async';
import 'dart:io';

import 'package:as2in1_app/const/colors_const.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WidgetBottomSheetVidio extends StatefulWidget {
  final String? url;
  const WidgetBottomSheetVidio({this.url});

  @override
  _WidgetBottomSheetVidioState createState() => _WidgetBottomSheetVidioState();
}

class _WidgetBottomSheetVidioState extends State<WidgetBottomSheetVidio> {
  final Completer<WebViewController> _controller = Completer<WebViewController>();
  bool isLoading = true;
  int progress = 0;
  final Set<Factory> gestureRecognizers = [Factory(() => EagerGestureRecognizer()),].toSet();

  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return widgetGridMenuPurchase();
  }

  Widget widgetGridMenuPurchase(){
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Stack(
        children: [
          WebView(
            initialUrl: widget.url,
            gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>{
                Factory<VerticalDragGestureRecognizer>(
                () => VerticalDragGestureRecognizer(),
                ),
              },
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) {
              _controller.complete(webViewController);
            },
            javascriptChannels: <JavascriptChannel>{
              _toasterJavascriptChannel(context),
            },
            onProgress: (int progress) {
              print("WebView is loading (progress : $progress%)");
              setState(() {
                this.progress = progress;
              });
            },
            onPageStarted: (String url) {
              print('Page started loading: $url');
            },
            onPageFinished: (String url) {
              print('Page finished loading: $url');
              setState(() {
                Future.delayed(Duration(milliseconds: 100));
                this.isLoading =  false;
              });
            },
            gestureNavigationEnabled: true,
          ),
          this.isLoading ?  Center(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: LinearPercentIndicator(
                width: MediaQuery.of(context).size.width - 50,
                animation: true,
                lineHeight: 20.0,
                animationDuration: 2500,
                percent: 1.0,
                center: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      text:"$progress%",
                      style: TextStyle(color: Colors.white,fontSize: 12.0),
                  ),
                ),
                linearStrokeCap: LinearStrokeCap.roundAll,
                progressColor: HexColor(COLOR_RED),
              ),
            ),
          ) : Stack()
        ],
      ),
    );
  }
  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          // ignore: deprecated_member_use
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        });
  }

}

class Choice {
  const Choice({required this.title, required this.icon});
  final String title;
  final String icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Customer Service', icon: "assets/service/customer_service_v3.png"),
  const Choice(title: 'Referral Program', icon: "assets/service/referal_program.png"),
  const Choice(title: 'Apply Referral', icon: "assets/service/apply_refereal_v2.png"),
  const Choice(title: 'ID Registration', icon: "assets/service/id_registration.png"),
  const Choice(title: 'Vidio', icon: "assets/service/vidio.png"),
  const Choice(title: 'INDOSOUND', icon: "assets/service/IndoSound.png"),
  const Choice(title: 'KlikDokter', icon: "assets/service/klik_dokter.png"),
  const Choice(title: 'Liputan 6', icon:"assets/service/liputan6_v2.png"),
  const Choice(title: 'Bola', icon:"assets/service/bola_v2.png"),
  const Choice(title: 'Bintang', icon: "assets/service/bintangcom.png"),
  const Choice(title: 'Liputan BMI', icon: "assets/service/logo.png"),
  const Choice(title: 'Apakabar', icon: "assets/service/logo_akol_circle.png"),
  const Choice(title: 'DDHK-Website', icon: "assets/service/DDHK_Website.png"),
];


class SelectCard extends StatelessWidget {
  const SelectCard({required Key key, required this.choice}) : super(key: key);
  final Choice choice;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = TextStyle(fontSize: 18.0,color: Colors.black);
    return Container(
      padding: EdgeInsets.all(5),
        child: Center(child: ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: <Widget>[
              Image.asset(choice.icon,width: 40,height: 40),
              SizedBox(height: 10,),
              Text(choice.title, style: textStyle,textAlign: TextAlign.center,),
              SizedBox(height: 10,),
            ]
        ),
        )
    );
  }
}
