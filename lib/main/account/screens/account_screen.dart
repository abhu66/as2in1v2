import 'package:as2in1_app/const/colors_const.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AccountScreen extends StatefulWidget {

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            Image.asset("assets/ic_slice_top.png",fit: BoxFit.cover,width: 250),
            ListView(
              children: [
                widgetHeader(),
                SizedBox(height: 10,),
                Container(
                    height: 5,
                    decoration: BoxDecoration(
                      color: HexColor("#F0F8FF"),
                      borderRadius: BorderRadius.circular(10),
                    ),
                ),
                widgetSetting(),
                widgeMyPackage(),
                widgetShare(),
                widgetTestCallFunction(),
                widgetChat(),
                widgetRedownloads(),
                Container(
                  height: 5,
                  decoration: BoxDecoration(
                    color: HexColor("#F0F8FF"),
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                widgetLogout(),
              ],
            ),
          ],
        ),
      ),
    );
  }


  Widget widgetHeader(){
    return Container(
      margin: EdgeInsets.only(top: 60,),
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: [
          ListTile(
            leading:ClipOval(
              child: ClipOval(
                child: Image.asset(
                  "assets/ic_as_red.png",
                  width: 40,
                  height: 40,
                ),
              ),
            ),
            title: RichText(
              textAlign: TextAlign.left,
              text: TextSpan(
                text: "Abu Khoerul Iskandar Ali",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w700),
              ),
            ),
            subtitle:RichText(
            textAlign: TextAlign.left,
              text: TextSpan(
                text: "081218209581",
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 12,
                    fontWeight: FontWeight.w700),
              ),
            ),
          ),

          Container(
            margin: EdgeInsets.only(left: 20,right: 30),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.greenAccent.withOpacity(0.5)
            ),
            child: ListTile(
              leading: Image.asset("assets/icons/wallet.png",width: 40,height: 40,),
              title:  RichText(
                textAlign: TextAlign.left,
                text: TextSpan(
                    text: "\$",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                    children: [
                      TextSpan(
                          text: "20.000",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.bold)
                      ),
                    ]
                ),
              ),
              subtitle: RichText(
                textAlign: TextAlign.left,
                text: TextSpan(
                  text: "Credit Balance",
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 14,
                      fontWeight: FontWeight.bold),
                ),
              ),
              trailing: Container(
                  height: 30,
                  width: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),

                        gradient: LinearGradient(
                          colors: [
                            HexColor("#891F1F"),
                            HexColor("#B11B33"),
                          ],
                        ),
                  ),
                  child: Center(child: Icon(Icons.add,size: 25,color: Colors.white,))),
            ),
            // child: ListTile(
            //   leading: Image.asset("assets/icons/wallet.png",width: 40,height: 40,),
            //   title: RichText(
            //     textAlign: TextAlign.left,
            //     text: TextSpan(
            //       text: "Abu Khoerul Iskandar Ali",
            //       style: TextStyle(
            //           color: Colors.black,
            //           fontSize: 14,
            //           fontWeight: FontWeight.w700),
            //     ),
            //   ),
            //   subtitle:RichText(
            //     textAlign: TextAlign.left,
            //     text: TextSpan(
            //       text: "081218209581",
            //       style: TextStyle(
            //           color: Colors.grey,
            //           fontSize: 12,
            //           fontWeight: FontWeight.w700),
            //     ),
            //   ),
            // ),
          ),
        ],
      ),
    );
  }


  Widget widgetPersonalData(){
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: ListTile(
        leading: Container(
          height: 40,
          width: 40,
          decoration: BoxDecoration(
            color: HexColor("#F0F8FF"),
            borderRadius: BorderRadius.circular(10)
          ),
            child: Center(child: Icon(Icons.account_box_rounded,size: 25,color: Colors.black54,))),
        title: RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
            text: "Personal Data",
            style: TextStyle(
                color: Colors.black,
                fontSize: 12,
                fontWeight: FontWeight.w700),
          ),
        ),
        trailing: IconButton(icon: Icon(Icons.keyboard_arrow_right_rounded),onPressed: (){},),
      ),
    );
  }

  Widget widgetSetting(){
    return Container(
      margin: EdgeInsets.only(top: 0),
      child: ListTile(
        leading: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                color: HexColor("#F0F8FF"),
                borderRadius: BorderRadius.circular(10)
            ),
            child: Center(child: Image.asset("assets/icons/settings.png",height: 25,width: 25,),)),
        title: RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
            text: "Settings",
            style: TextStyle(
                color: Colors.black,
                fontSize: 12,
                fontWeight: FontWeight.w700),
          ),
        ),
        trailing: IconButton(icon: Icon(Icons.keyboard_arrow_right_rounded),onPressed: (){},),
      ),
    );
  }

  Widget widgeMyPackage(){
    return Container(
      margin: EdgeInsets.only(top: 0),
      child: ListTile(
        leading: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                color: HexColor("#F0F8FF"),
                borderRadius: BorderRadius.circular(10)
            ),
            child: Center(child: Icon(Icons.cases,size: 25,color: Colors.black54,))),
        title: RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
            text: "My Package",
            style: TextStyle(
                color: Colors.black,
                fontSize: 12,
                fontWeight: FontWeight.w700),
          ),
        ),
        trailing: IconButton(icon: Icon(Icons.keyboard_arrow_right_rounded),onPressed: (){},),
      ),
    );
  }

  Widget widgetShare(){
    return Container(
      margin: EdgeInsets.only(top: 0),
      child: ListTile(
        leading: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                color: HexColor("#F0F8FF"),
                borderRadius: BorderRadius.circular(10)
            ),
            child: Center(child: Image.asset("assets/icons/share.png",height: 25,width: 25,),)),
        title: RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
            text: "Share",
            style: TextStyle(
                color: Colors.black,
                fontSize: 12,
                fontWeight: FontWeight.w700),
          ),
        ),
        trailing: IconButton(icon: Icon(Icons.keyboard_arrow_right_rounded),onPressed: (){},),
      ),
    );
  }

  Widget widgetTestCallFunction(){
    return Container(
      margin: EdgeInsets.only(top: 0),
      child: ListTile(
        leading: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                color: HexColor("#F0F8FF"),
                borderRadius: BorderRadius.circular(10)
            ),
            child: Center(child: Image.asset("assets/icons/call.png",height: 25,width: 25,),)),
        title: RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
            text: "Test Call Function",
            style: TextStyle(
                color: Colors.black,
                fontSize: 12,
                fontWeight: FontWeight.w700),
          ),
        ),
        trailing: IconButton(icon: Icon(Icons.keyboard_arrow_right_rounded),onPressed: (){},),
      ),
    );
  }

  Widget widgetRedownloads(){
    return Container(
      margin: EdgeInsets.only(top: 0),
      child: ListTile(
        leading: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                color: HexColor("#F0F8FF"),
                borderRadius: BorderRadius.circular(10)
            ),
            child: Center(child: Image.asset("assets/icons/redownload.png",height: 25,width: 25,),)),
        title: RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
            text: "Re-download Phone Book",
            style: TextStyle(
                color: Colors.black,
                fontSize: 12,
                fontWeight: FontWeight.w700),
          ),
        ),
        trailing: IconButton(icon: Icon(Icons.keyboard_arrow_right_rounded),onPressed: (){},),
      ),
    );
  }

  Widget widgetChat(){
    return Container(
      margin: EdgeInsets.only(top: 0),
      child: ListTile(
        leading: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                color: HexColor("#F0F8FF"),
                borderRadius: BorderRadius.circular(10)
            ),
            child: Center(child: Image.asset("assets/icons/chat_rounded.png",height: 25,width: 25,),)),
        title: RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
            text: "Send Us Trace Logs",
            style: TextStyle(
                color: Colors.black,
                fontSize: 12,
                fontWeight: FontWeight.w700),
          ),
        ),
        trailing: IconButton(icon: Icon(Icons.keyboard_arrow_right_rounded),onPressed: (){},),
      ),
    );
  }

  Widget widgetLogout(){
    return Container(
      margin: EdgeInsets.only(top: 0),
      child: ListTile(
        leading: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                color: HexColor("#F0F8FF"),
                borderRadius: BorderRadius.circular(10)
            ),
            child: Center(child: Image.asset("assets/icons/redownload.png",height: 25,width: 25,),)),
        title: RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
            text: "Log Out",
            style: TextStyle(
                color: Colors.black,
                fontSize: 12,
                fontWeight: FontWeight.w700),
          ),
        ),
        trailing: IconButton(icon: Icon(Icons.keyboard_arrow_right_rounded),onPressed: (){},),
      ),
    );
  }
}
