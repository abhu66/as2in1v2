
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetBottomSheetVoiceAndSms extends StatefulWidget {


  @override
  _WidgetBottomSheetVoiceAndSmsState createState() => _WidgetBottomSheetVoiceAndSmsState();
}

class _WidgetBottomSheetVoiceAndSmsState extends State<WidgetBottomSheetVoiceAndSms> {
  Widget build(BuildContext context) {
    return widgetGridMenuPurchase();
  }

  Widget widgetGridMenuPurchase(){
    return GridView.count(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.all(10),
        crossAxisCount: 4,
        crossAxisSpacing: 4.0,
        mainAxisSpacing: 8.0,
        children: List.generate(
            choicesVoiceAndSms.length, (index) {
          return Center(
            child: SelectCard(choice: choicesVoiceAndSms[index],index: index,key: UniqueKey(),),
          );
        }
        )
    );
  }
}

class Choice {
  const Choice({required this.title, required this.icon});
  final String title;
  final String icon;
}

const List<Choice> choicesVoiceAndSms = const <Choice>[
  const Choice(title: 'TM Bebas (120 mins)', icon: "assets/icons/call.png"),
  const Choice(title: 'TM Bebas (50 mins)', icon: "assets/icons/call.png"),
  const Choice(title: 'SMS 35', icon: "assets/icons/chat_rounded.png"),
  const Choice(title: 'SMS 75', icon: "assets/icons/chat_rounded.png"),
];


class SelectCard extends StatelessWidget {
  const SelectCard({required Key key, required this.choice,required this.index}) : super(key: key);
  final Choice choice;
  final int index;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = TextStyle(fontSize: 18.0,color: Colors.black);
    return InkWell(
      child: Container(
          padding: EdgeInsets.all(5),
          child: Center(child: ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                Image.asset(choice.icon,width: 40,height: 40),
                SizedBox(height: 10,),
                Text(choice.title, style: textStyle,textAlign: TextAlign.center,),
                SizedBox(height: 10,),
              ]
          ),
          )
      ),
      onTap: (){
        showModalBottomSheet(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  topLeft: Radius.circular(30)
              ),
            ),
            backgroundColor: Colors.white,
            isScrollControlled: true,
            context: context, builder: (BuildContext context){
          return Container(
              height: MediaQuery.of(context).size.height - AppBar().preferredSize.height,
              child: ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  InkWell(
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Spacer(),
                          RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                                text:choice.title,
                                style:TextStyle(fontSize: 18.0,color: Colors.black)
                            ),
                          ),
                          Spacer(),
                          Icon(Icons.clear,size: 35,)
                        ],
                      ),
                      margin: EdgeInsets.all(20),
                    ),
                    onTap: (){
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ));
        }
        );
      },
    );
  }
}
