import 'dart:async';
import 'dart:developer';

import 'package:as2in1_app/const/const_string.dart';
import 'package:as2in1_app/main/screens/main_screen.dart';
import 'package:as2in1_app/shared_prefs/shared_prefs.dart';
import 'package:as2in1_app/splash/screen/images/const_images_splash.dart';
import 'package:as2in1_app/started/screens/started_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  double opacityLevel = 0.0;
  @override
  void initState() {
    super.initState();
    startSplashScreen();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Image.asset("assets/ic_slice_top.png",fit: BoxFit.cover,width: 250),
          Container(
            width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(left: 50,right: 50),
              child:Center(
                child: ListView(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  children: [
                    Image.asset("assets/ic_splash.png",height: 250,),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text:'${ConstStringBahasa.welcome}',
                        style: TextStyle(fontSize: 20,color: Colors.black),
                      ),
                    ),
                  ],
                ),
              )
          ),
        ],
      ),
    );
  }

  startSplashScreen() async {
    var duration = const Duration(seconds: 3);
    return Timer(duration,() async {
     SharedPref().isFirstTime().then((value) {
       log("YA $value");
       // ignore: unnecessary_null_comparison
       if(value != null){
         if(value != "Y"){
           goToMain();
         }
         else {
           SharedPref.saveToken("first", "Y");
           goToStarted();
         }
       }
       else {
         SharedPref.saveToken("first", "Y");
         goToStarted();
       }
      }).catchError((error){
        SharedPref.saveToken("first", "Y");
        goToStarted();
     });
    });
  }

  goToMain() async {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (_){
        return MainScreen();
      }),
    );
  }

  goToStarted() async {
    if(mounted)
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (_){
        return StartedScreen();
      }),
    );
  }
}
