import 'dart:async';
import 'dart:developer';
import 'package:as2in1_app/const/colors_const.dart';
import 'package:as2in1_app/main/screens/main_screen.dart';
import 'package:as2in1_app/shared_prefs/shared_prefs.dart';
import 'package:as2in1_app/started/screens/otp_succes_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class OTPScreen extends StatefulWidget {
  @override
  _OTPScreenState createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  double opacityLevel = 0.0;
  TextEditingController _phoneNumberController0 = TextEditingController();
  TextEditingController _phoneNumberController1 = TextEditingController();
  TextEditingController _phoneNumberController2 = TextEditingController();
  TextEditingController _phoneNumberController3 = TextEditingController();
  TextEditingController _phoneNumberController4 = TextEditingController();

  bool isValidButton = false;

  @override
  void initState() {
    super.initState();
    //startSplashScreen();
  }
  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Icon(Icons.arrow_back_rounded,color: Colors.white,),
        actions: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Icon(Icons.help_rounded,color: Colors.grey,),
          )
        ],
      ),
      body: Container(
        child: ListView(
          padding: EdgeInsets.only(top: 0),
          physics: BouncingScrollPhysics(),
          children: [
            Stack(
              children: [
                Image.asset("assets/ic_slice_top.png",fit: BoxFit.cover,width: 250),

              ],
            ),
            Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(left: 50,right: 50),
                child:Center(
                  child: ListView(
                    physics: BouncingScrollPhysics(),
                    shrinkWrap: true,
                    children: [
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text:'Enter your OTP below',
                        ),
                      ),

                      SizedBox(height: 30,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Flexible(
                            child: Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10)
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.9),
                                    spreadRadius:1.5,
                                    blurRadius: 1.5,
                                    offset: Offset(1, 0), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Center(
                                child: widgetInputFeldApp(
                                    field: "",
                                    node: node,
                                    label: "",
                                    controller: _phoneNumberController0,
                                    hint: "",
                                    textInputType: TextInputType.number,
                                    decoration: decoration(
                                        hint:"",
                                        label:"",
                                        asset:""
                                    )
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            child: Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10)
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.9),
                                    spreadRadius:1.5,
                                    blurRadius: 1.5,
                                    offset: Offset(1, 0), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Center(
                                child: widgetInputFeldApp(
                                    field: "",
                                    node: node,
                                    label: "",
                                    controller: _phoneNumberController1,
                                    hint: "",
                                    textInputType: TextInputType.number,
                                    decoration: decoration(
                                        hint:"",
                                        label:"",
                                        asset:""
                                    )
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            child: Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10)
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.9),
                                    spreadRadius:1.5,
                                    blurRadius: 1.5,
                                    offset: Offset(1, 0), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Center(
                                child: widgetInputFeldApp(
                                    field: "",
                                    node: node,
                                    label: "",
                                    controller: _phoneNumberController2,
                                    hint: "",
                                    textInputType: TextInputType.number,
                                    decoration: decoration(
                                        hint:"",
                                        label:"",
                                        asset:""
                                    )
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            child: Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10)
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.9),
                                    spreadRadius:1.5,
                                    blurRadius: 1.5,
                                    offset: Offset(1, 0), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Center(
                                child: widgetInputFeldApp(
                                    field: "",
                                    node: node,
                                    label: "",
                                    controller: _phoneNumberController3,
                                    hint: "",
                                    textInputType: TextInputType.number,
                                    decoration: decoration(
                                        hint:"",
                                        label:"",
                                        asset:""
                                    )
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            child: Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10)
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.9),
                                    spreadRadius:1.5,
                                    blurRadius: 1.5,
                                    offset: Offset(1, 0), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Center(
                                child: widgetInputFeldApp(
                                    field: "",
                                    node: node,
                                    label: "",
                                    controller: _phoneNumberController4,
                                    hint: "",
                                    textInputType: TextInputType.number,
                                    decoration: decoration(
                                        hint:"",
                                        label:"",
                                        asset:""
                                    )
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 30,),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text:'Didn\'t receive your OTP ',
                            children: [
                              TextSpan(
                                text:'click here.',
                                 ),
                            ]
                        ),
                      ),
                      SizedBox(height: 30,),
                      Image.asset("assets/ic_register_ava.png",height: 250,),
                      buttonNext(),
                    ],
                  ),
                )
            ),
          ],
        ),
      ),
    );
  }

  startSplashScreen() async {
    var duration = const Duration(seconds: 3);
    return Timer(duration,() async {
     SharedPref().isFirstTime().then((value) {
       log("YA $value");
       // ignore: unnecessary_null_comparison
       if(value != null){
         if(value != "Y"){
           goToMain();
         }
         else {
           SharedPref.saveToken("first", "Y");
           goToOTPSuccess();
         }
       }
       else {
         SharedPref.saveToken("first", "Y");
         goToOTPSuccess();
       }
      }).catchError((error){
        SharedPref.saveToken("first", "Y");
        goToOTPSuccess();
     });
    });
  }

  goToMain() async {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (_){
        return MainScreen();
      }),
    );
  }

  goToOTPSuccess() async {
    if(mounted)
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (_){
        return OTPSuccessScreen();
      }),
    );
  }

  Widget widgetInputFeldApp({required String label,required String field, required TextInputType textInputType,required String hint,
    required InputDecoration decoration,required TextEditingController controller, required FocusScopeNode node}) {
    return TextFormField(
      autofocus: true,
      textAlign: TextAlign.center,
      controller: controller,
      cursorColor: HexColor(COLOR_GREY),
      style: TextStyle(
          fontFamily: 'Poppins-Regular',
          fontSize: 16,
          color: HexColor(COLOR_RED),
          fontWeight: FontWeight.w700),
      keyboardType: textInputType,
      // validator: (value) {
      //   return Validators().validateFieldGeneral(label: label,value: value);
      // },
      inputFormatters: <TextInputFormatter>[
        LengthLimitingTextInputFormatter(1),
      ],
      onChanged: (value){
        setState(() {
          if(value.length == 1){
            node.nextFocus();
          }
          checkValidOtp();
        });
      },
      decoration: decoration,
    );
  }

  InputDecoration decoration({required String label,required String hint,required String asset}){
    return InputDecoration(
      labelText: label,
      labelStyle: TextStyle(
          fontFamily: 'Poppins-Regular',
          color: HexColor(COLOR_GREY),
          fontSize: 18,
          fontWeight: FontWeight.w300),
      hintText: hint,
      enabledBorder: InputBorder.none,
      focusedBorder: InputBorder.none,
      border: InputBorder.none,
      // prefixIcon: Padding(
      //     padding: EdgeInsets.only(right: 5, bottom: 0, top: 0),
      //     child: Image.asset(
      //       asset,
      //       scale: 4,
      //     )),
    );
  }

  Widget buttonNext(){
    return InkWell(
      onTap: !isValidButton ? null : (){
       goToOTPSuccess();
      },
      child: Container(
        height: 45,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: isValidButton ? HexColor(COLOR_RED) : Colors.grey,
        ),
        child: Center(
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                text:'Submit',
                style: Theme.of(context).textTheme.headline1!.copyWith(fontSize: 14,color: Colors.white)
            ),
          ),
        ),
      ),
    );
  }


  checkValidOtp(){
    if(this._phoneNumberController0.value.text.isEmpty  ||
        this._phoneNumberController1.value.text.isEmpty ||
        this._phoneNumberController2.value.text.isEmpty ||
        this._phoneNumberController3.value.text.isEmpty ||
        this._phoneNumberController4.value.text.isEmpty){
        setState(() {
          this.isValidButton = false;
        });
    }
    else {
      setState(() {
        this.isValidButton = true;
      });
    }
  }
}
