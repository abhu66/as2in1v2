
import 'package:as2in1_app/const/colors_const.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetRecommendation extends StatefulWidget {
  @override
  _WidgetRecommendationState createState() => _WidgetRecommendationState();
}

class _WidgetRecommendationState extends State<WidgetRecommendation> {

  int currentIndex = 0;

  List cardList=[
    Item1(),
    Item1(),
    Item1(),
    Item1()
  ];

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 220,
            margin: EdgeInsets.only(
                top:20,
                left: 10,
                right: 10
            ),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20)
            ),
            // child: widgetRecommendation()
      ),
        Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20)
          ),
          margin: EdgeInsets.only(top: 70),
          child: CarouselSlider(
            options: CarouselOptions(
              height: 135.0,
              autoPlay: true,
              autoPlayInterval: Duration(seconds: 3),
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              autoPlayCurve: Curves.fastOutSlowIn,
              pauseAutoPlayOnTouch: true,
              aspectRatio: 2.0,
              onPageChanged: (index, reason) {
                setState(() {
                  currentIndex = index;
                });
              },
            ),
            items: cardList.map((card){
              return Builder(
                  builder:(BuildContext context){
                    return Container(
                        margin: EdgeInsets.only(right: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        height: MediaQuery.of(context).size.height*0.30,
                        width: MediaQuery.of(context).size.width,
                        child: Image.asset("assets/images/ic_slide_1.png",fit: BoxFit.scaleDown,)
                    );
                  }
              );
            }).toList(),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 210,left: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start  ,
            children: map<Widget>(cardList, (index, url) {
              return Container(
                width: 10.0,
                height: 10.0,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: currentIndex == index ? HexColor("#FF0000") : Colors.grey,
                ),
              );
            }),
          ),
        ),
      ],
    );
  }

  // Widget widgetRecommendation(){
  //   return Stack(
  //     children: [
  //       Container(
  //         decoration: BoxDecoration(
  //             color: Colors.white,
  //             borderRadius: BorderRadius.only(
  //               bottomLeft: Radius.circular(20),
  //               bottomRight: Radius.circular(20)
  //             )
  //         ),
  //         margin: EdgeInsets.only(
  //             top: 20,
  //             left: 10,
  //             right: 10
  //         ),
  //         child: Column(
  //           crossAxisAlignment: CrossAxisAlignment.start,
  //           children: [
  //             Container(
  //               margin: EdgeInsets.only(right: 16.0),
  //               child: Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                 children: [
  //                   RichText(
  //                     textAlign: TextAlign.left,
  //                     text: TextSpan(
  //                         text:'Recommendation',
  //                         style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 20)
  //                     ),
  //                   ),
  //                   InkWell(
  //                     onTap: (){},
  //                     child: RichText(
  //                       textAlign: TextAlign.left,
  //                       text: TextSpan(
  //                           text:'See All',
  //                           style: Theme.of(context).textTheme.headline1.copyWith(fontSize: 14,color: Colors.red)
  //                       ),
  //                     ),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //       ),
  //     ],
  //   );
  // }
}

class Item1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.3, 1],
            colors: [Color(0xffff4000),Color(0xffffcc66),]
        ),
      ),
      child: Image.asset("assets/images/ic_slide_1.png",fit: BoxFit.scaleDown,)
    );
  }
}
