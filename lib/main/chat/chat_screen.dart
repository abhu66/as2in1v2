
import 'package:as2in1_app/const/colors_const.dart';
import 'package:as2in1_app/main/chat/widget_chat_sms.dart';
import 'package:as2in1_app/main/home/keypad_sms.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  TextEditingController _searchController = TextEditingController();

  List<String> menu = [
    "SMS",
    "Online Chat"
  ];

  late int currentIndex = 0;
  late String querySearch = "";
  bool isCreateNewChat = false;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0.5,
        centerTitle: false,
        title: RichText(
          textAlign: TextAlign.left,
          text: TextSpan(
              text: 'Conversation',
              style:TextStyle(color: Colors.black,fontSize: 26,fontWeight: FontWeight.bold)),
        ),
        backgroundColor: Colors.white,
        actions: [
          InkWell(
            onTap: (){
              setState(() {
                this.isCreateNewChat = !this.isCreateNewChat;
              });
            },
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                padding: EdgeInsets.only(left: 8, right: 8, top: 2, bottom: 2),
                height: 20,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.pink[50],
                ),
                child: isCreateNewChat ? Row(
                  children: <Widget>[
                    Text(
                      "Cancel",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.green),
                    ),
                  ],
                ) : Row(
                  children: <Widget>[
                    Icon(
                      Icons.add,
                      color: Colors.pink,
                      size: 20,
                    ),
                    SizedBox(
                      width: 2,
                    ),
                    Text(
                      "Add New",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.green),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
      body: isCreateNewChat ? KeypadSms() : Container(
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: ListView(
            children: [
              Row(
                children: [
                  Flexible(
                    child: Padding(
                      padding: EdgeInsets.only(top: 10, left: 16, right: 16),
                      child: Container(
                        height: 40,
                        child: TextField(
                          controller: _searchController,
                          onChanged: (value){
                            setState(() {
                              this.querySearch = value;
                            });
                          },
                          decoration: InputDecoration(
                            hintText: "Search...",
                            hintStyle: TextStyle(color: Colors.grey.shade600),
                            prefixIcon: Icon(
                              Icons.search,
                              color: Colors.grey.shade600,
                              size: 20,
                            ),
                            filled: true,
                            fillColor: Colors.grey.shade100,
                            contentPadding: EdgeInsets.all(10),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide:
                                    BorderSide(color: Colors.grey.shade100)),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide:
                                    BorderSide(color: Colors.grey.shade100)),
                          ),
                        ),
                      ),
                    ),
                  ),
                  PopupMenuButton(
                    onSelected:(int value){
                      setState(() {
                        this.currentIndex = value;
                      });
                    },
                    initialValue: currentIndex,
                    child: Icon(Icons.filter_list_outlined,size: 35,),
                    itemBuilder: (context) {
                      return List.generate(menu.length, (index) {
                        return PopupMenuItem(
                          value: index,
                          child: Text(menu[index]),
                        );
                      });
                    },
                  ),
                  SizedBox(
                    width: 20.0,
                  ),
                ],
              ),
              currentIndex == 0 ? WidgetchatSMSContainer(querySearch: querySearch,) : Container(),
            ],
          )),
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      // floatingActionButton: FloatingActionButton(
      //   child: Image.asset("assets/images/ic_dial.png"),
      // ),
    );
  }

  Widget serachField() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 40,
        margin: EdgeInsets.only(left: 10, right: 0),
        width: 350,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 0,
              offset: Offset(0, 0), // changes position of shadow
            ),
          ],
        ),
        child: Center(
          child: widgetInputFeldApp(
              label: "",
              controller: _searchController,
              hint: "",
              textInputType: TextInputType.number,
              decoration: decoration(hint: "Chat", label: "", asset: "")),
        ),
        padding: EdgeInsets.only(left: 20),
      ),
    );
  }

  Widget widgetInputFeldApp(
      {required String label,
        required TextInputType textInputType,
        required String hint,
        required InputDecoration decoration,
        required TextEditingController controller}) {
    return TextFormField(
      autofocus: true,
      controller: controller,
      cursorColor:Colors.grey,
      style: TextStyle(
          fontFamily: 'Poppins-Regular',
          fontSize: 14,
          fontWeight: FontWeight.w300),
      keyboardType: textInputType,
      // validator: (value) {
      //   return Validators().validateFieldGeneral(label: label,value: value);
      // },
      onChanged: (value) {
        setState(() {
          if (this._searchController.value.text.isEmpty) {
          } else {}
        });
      },
      decoration: decoration,
    );
  }

  InputDecoration decoration({required String label, required String hint, required String asset}) {
    return InputDecoration(
      labelText: label,
      labelStyle: TextStyle(
          fontFamily: 'Poppins-Regular',
          color: Colors.grey,
          fontSize: 18,
          fontWeight: FontWeight.w300),
      hintText: hint,
      enabledBorder: InputBorder.none,
      focusedBorder: InputBorder.none,
      border: InputBorder.none,
      // prefixIcon: Padding(
      //     padding: EdgeInsets.only(right: 5, bottom: 0, top: 0),
      //     child: Image.asset(
      //       asset,
      //       scale: 4,
      //     )),
    );
  }
}
